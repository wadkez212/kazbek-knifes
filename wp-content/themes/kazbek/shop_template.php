<?php
/*
Template Name: single product template
*/
?>

<main id="primary" class="site-main">

<section class="product-single">
    <div class="containers">
				<div class="title-product">
					<h1 class="title">Нож Ganzo G7362 черный, из кованой стали X12МФ</h1>
				</div>

				<div class="single-product" style="display: flex">
					
					<div class="single_product_gallery">

						<div class="product_foto_block">
						    <!-- фото товара -->
						   <?php do_action( 'woocommerce_before_single_product_summary' ); ?>
						</div>

						<div class="single-gallery">
							<img src="img/gallery-img.png" alt="">
							<img src="img/gallery-img.png" alt="">
							<img src="img/gallery-img.png" alt="">
						</div>
					</div>

					<div class="short_description">
                        <?php /*do_action( 'woocommerce_single_product_summary' );*/ ?>
						<div class="single-properties">
							<span class="name-propertires">Марка стали</span>
							<div></div>
							<span>Дамасская сталь</span>
						</div>

						<div class="single-properties">
							<span class="name-propertires">Общая длина</span>
							<div></div>
							<span>275 мм</span>
						</div>

						<div class="single-properties">
							<span class="name-propertires">Длина клинка</span>
							<div></div>
							<span>140 мм</span>
						</div>

						<div class="single-properties">
							<span class="name-propertires">Ширина клинка</span>
							<div></div>
							<span>25 мм</span>
						</div>

						<div class="single-properties">
							<span>Толщина обуха</span>
							<div></div>
							<span>2,4 мм</span>
						</div>

						<div class="single-properties">
							<span class="name-propertires">Толщина обуха</span>
							<div></div>
							<span>2,4 мм</span>
						</div>

						<div class="single-properties">
							<span class="name-propertires">Толщина обуха</span>
							<div></div>
							<span>2,4 мм</span>
						</div>

						<div class="single-properties">
							<span class="name-propertires">Толщина обуха</span>
							<div></div>
							<span>2,4 мм</span>
						</div>

						<div class="single-info">Доступен под заказ <img src="img/single-questions.svg" alt=""></div>
						<div class="single-price">13 560 ₽</div>

						<div class="block-single-cart">
							<button class="add-card"><img class="cart_icon" src="img/cart-icons.svg">В корзину</button>
							<img class="single-calc" src="img/calc.png" alt="" width="32" height="32">
						</div>

						<div class="single-text">Если у вас появились вопросы позвоните нам <br> по телефону 8 (800) 123-45-67</div>
						
					</div> 
					
				</div>
			</section>

			<section class="desctiption_single_product">
				<div class="desctiption">
					<p class="single-description-title">Описание товара</p>
					<span class="single-description">Авторский нож "Викинг". Клинок откован из фирменной дамасской стали многослойностью 1200 слоёв. Рукоять изготовлена из стабилизированного чёрного граба, художественного литья мельхиор.
						Художественное литьё имеет главный символ Советской армии в виде пятиконечной звезды.
						На пятке рукояти изображён снаряд пробивающий броню, как олицетворение военных лет второй мировой!
						Вес ножа (без ножен): 200 грамм
						
						Комплектация:
						
						1) Ножны из натуральной кожи.
						2) Сертификат соответствия (подтверждающий, что нож не является холодным оружием).</span>
				</div>

				<button class="brand">Доставка</button>
				<button class="brand">Оплата</button>
				<button class="brand">Гарантия и возврат</button>
				</div>
			</section>
			
		        <div>
			   <?php do_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs'); ?>
			    </div>
			
			
			</main>
