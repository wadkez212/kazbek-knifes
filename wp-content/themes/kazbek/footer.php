<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Kazbek
 */

?>

<footer class="footer" style="background-image: url(<?php the_field('topor', 'options'); ?>);">
            <div class="containers">
                <div class="block">
                    <div class="footer_logo_block"><a href="#"><img src="<?php the_field('footer_logo', 'options'); ?>" alt=""></a></div>
                    <div class="footer_navigation"><a href="#">Политика конфиденциальности</a></div>
                    <div class="footer_navigation"><a href="#">Договор оферты</a></div>
                    <div class="copirate">2022 © Казбек. Все права защищены</div>
                </div>
                <div class="block">
                    <div class="footer_block_title">
                        <?php the_field('footer_second_title', 'options'); ?>
                    </div>
                    
                                              <!-- НАТЯЖКА МЕНЮ !  -->
<?php wp_nav_menu( [
	'theme_location'  => 'menu-2',
	'container'       => null,
	'menu_class'      => 'footer-menu',
	'menu_id'         => '',
	'echo'            => true,
	'fallback_cb'     => 'wp_page_menu',
	'before'          => '',
	'after'           => '',
	'link_before'     => '',
	'link_after'      => '',
	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	'walker'          => ''
] );
?>
                   <!-- <ul>
                        <li><a href="#">О мастерской</a></li>
                        <li><a href="#">Доставка</a></li>
                        <li><a href="#">Оплата</a></li>
                        <li><a href="#">Гарантия и возврат</a></li>
                        <li><a href="#">Оптовым клиентам</a></li>
                        <li><a href="#">Сертификаты</a></li>
                    </ul> --> 
                </div>
                <div class="block">
                    <div class="footer_block_title">
                        <?php the_field('footer_third_title', 'options'); ?>
                    </div>
                    <a href="tel:88001234567" class="footer_phone"><?php the_field('footer_tel', 'options'); ?></a>
                    <div class="footer_social_block">
                        <div class="block"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/vk.png" alt=""></a></div>
                        <div class="block"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/youtube.png" alt=""></a></div>
                    </div>
                    <div class="location">
                        <img src="<?php the_field('footer_icon_location', 'options'); ?>" alt="location icon">
                        
                        <span>
                            <?php the_field('footer_location_text', 'options'); ?>
                        </span>
                    </div>
                    <div class="footer_mail_block">
                        <img src="<?php the_field('footer_icon_mail', 'options'); ?>" alt="">
                        <?php the_field('footer_mail_text','options'); ?>
                    </div>
                    <img src="<?php echo get_site_url(); ?>/wp-content/themes/kazbek/assets/img/ferz.png" alt=""/>
                </div>
            </div>
        </footer>

    </main>
    
    <?php wp_footer(); ?>
    
    <script src="<?php get_site_url(); ?>/wp-content/themes/kazbek/assets/js/main.js"></script>
    <!-- <script src="js/main.js"></script>
    <script src="js/jq.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/slider.js"></script> -->
</body>
</html>
