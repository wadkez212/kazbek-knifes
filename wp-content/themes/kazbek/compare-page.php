<?php

/**
 * Theme Name: kazbek
 * The template for displaying all pages
 * Template Name: Compare
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package kazbek
 */

session_start();
$_SESSION['stop'] = 0;

get_header();
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/mobile-detect/1.4.4/mobile-detect.min.js"></script>
<script>
    let detect = new MobileDetect(window.navigator.userAgent)
</script>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Сравнение</title>
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"
    />
    <!-- Link Swiper's CSS -->
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css"
    />
  </head>

	<main id="primary" class="site-main">
    <div class="containers">
      <h1><?php echo get_the_title(); ?></h1>
      <div class="swiper compara">
          <div class="swiper-wrapper">
              
              <?php
                  session_start();
                  $items = $_SESSION['compare'];
        
                  $i = 0;

                  $count = count($_SESSION['compare']);
                  if($count == 0){
                    echo "Нет товаров для сравнения";
                  }

                  if($count == 1){
                    echo "Добавьте более одного товара для сравнения";
                  }

                  if($count > 1) {
                    foreach($items as $item){
                        $product = wc_get_product( $item );
                ?>
                <div class='swiper-slide'>

                        <button type="button" class="close" data="<?php echo $product->id; ?>">
                            <span aria-hidden="true">×</span>
                        </button>

                        <a href="<?php echo $product->get_permalink($item); ?>">
                            <li class="catalog-gallery__item_compare" style="width: 100%!important">
                                    <!-- Изображение товара -->
                                    <div class="img_block" style="text-align: center">
                                        <?php	do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
                                    </div>
                                    <!-- Заголовок товара -->
                                    <h5 class="catalog-gallery__title"><?php echo $product->get_name(); ?></h5>

                                            <!-- Цена -->
                                            <p class="catalog-gallery__price">
                                                <?php if ( $price_html = $product->get_price_html() ) : ?>
                                                    <span class="price"><?php echo $price_html; ?></span>
                                                <?php endif; ?>
                                            </p>
                                            <!-- КНОПКА Добавить в корзину --> 
                                            <button class="catalog-gallery__btn"><?php do_action( 'woocommerce_after_shop_loop_item' ); ?></button>



                                            <div class="compare_slide compare_items">
                                                <?php 
                
                                                $attribute = $product->list_attributes();
                                                foreach($attribute as $attr){
                                                  ?>
                                                  
                                                    <?php
                                                      echo $attr; 
                                                    } ?>
          
                                                </div>
                            </li>
                        </a>

                    
                </div>
                        
                    <?php 
                    $i++;
                    
                    }
                  }
                
                  /*get_sidebar();*/
                  //get_footer();
                  ?>
                      
          </div>
          <!-- <div class="swiper-pagination"></div> -->
      </div>
    </div>




    <!-- Swiper JS -->
    <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>

    <!-- Initialize Swiper -->
    <script>

        let count= <?php echo count($_SESSION["compare"]); ?>;
         if(count >= 4) {
          count = 4;
         }
        let ww = jQuery(window).width();
        if(ww <= 1100) count = 3;
        if(ww <= 768) count = 2;

        console.log(count);

      var swiper = new Swiper(".compara", {
        slidesPerView: count,
        spaceBetween: 30,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
      });
    </script>
  </main>
</html>
<?php
 if(isset($_SESSION["compare"])){
   $cut = count($_SESSION["compare"]);
 }else{
   $cut = 0;
 }
 
 
 ?>

<script>

  let cut = <?php echo $cut ?>;
        if(cut > 4) cut = 4;
        var www = jQuery(window).width();
        if (www <= 1100) cut = 3;
        if (www <= 768) cut = 2;

      var swiper = new Swiper(".compara", {
        slidesPerView: cut,
        spaceBetween: 30,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
      });
      </script>





<script src="<?php echo get_site_url();?>/wp-content/themes/kazbek/assets/js/compare.js"></script>


<?php

get_footer();


