<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kazbek
 */

 session_start();
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">


	<?php wp_head(); ?>
    <script src="<?php echo content_url(); ?>/themes/kazbek/assets/js/main.js"> </script>  
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'kazbek' ); ?></a>

       <header class="header">
            <div class="containers">
                <div class="header_btn_menu">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/menuHamburger.png" alt="hamburger">
                    <span>Меню</span>
                </div>
                <div class="header__logo_block">
                    <a href="/">
                        <img src="<?php the_field('logo', 'options'); ?>" alt="logo">
                    </a>
                </div>
                <div class="header__right_block">
                    <div class="top_header_contacts">
                         <div class="top_header_contacts_location">
                             <img src="<?php the_field('header_location', 'options'); ?>" alt="location icon">
                            
                            <div>
                                <?php the_field('header_adress', 'options'); ?>
                            </div>
                         </div>
                         <div class="top_header_contacts_phone">
                             <a href="tel:88001234567" class="phone"><?php the_field('header_tel', 'options'); ?></a>
                             <a href="#" class="icons vk"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/vk.png" alt="vk logo"></a>
                             <a href="#" class="icons youtube"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/youtube.png" alt="youtube logo"></a>
                         </div>
                    </div>
                    <div class="bottom_header">
                        <nav class="header_nav">
                            <ul class="header_menu">
                                  <li>
                                    <button class="header_btn_catalog" style="margin-right: 40px;>
">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/hamburger.png" alt="hamburger">
                                        <span>Каталог</span>
                                    </button>
                                </li>
                                <?php wp_nav_menu( [
                                	'theme_location'  => 'menu-1',
                                	'container'       => null,
                                	'menu_class'      => 'header_menu',
                                	'menu_id'         => '',
                                	'echo'            => true,
                                	'fallback_cb'     => 'wp_page_menu',
                                	'before'          => '',
                                	'after'           => '',
                                	'link_before'     => '',
                                	'link_after'      => '',
                                	'items_wrap'      => '<ul id="%1$s" class="header_menu_links %2$s">%3$s</ul>',
                                	'depth'           => 0,
                                	'walker'          => ''
                                ] );
                                 ?>
                            </ul>
                        </nav>
                        <div class="header_cart_block">
                            <?php global $woocommerce; ?>
                            <a href="<?php echo get_site_url() ?>/compare">
                                <div class="comparison">
                                    <img src="<?php the_field('icon_compare', 'options'); ?>" alt="comp">
                                    <span class="website-nav__span website-nav__compare">Сравнение</span>
                                    <?php 
                                        if(isset($_SESSION['compare'])){
                                            ?>
                                     <a href="#" class="basket-btn basket-btn_fixed-xs">
                                        <span class="compare-btn__counter"><?php 
                                          echo count($_SESSION['compare']);  
                                          ?>
                                          </span>
                                    </a>
                                            <?php
                                        }else{
                                            ?>
                                    <a href="#" class="basket-btn ring basket-btn_fixed-xs">
                                        <span class="compare-btn__counter">     </span>                                      </span>
                                    </a>
                                            <?php
                                        }
                                          
                                                ?>
                              
                                   
                                </div>
                            </a>
                            <a href="<?php echo esc_url( wc_get_cart_url()) ?>">
                                <div class="cart">
                                    <img src="<?php the_field('icon_cart', 'options'); ?>" alt="cart icon">
                                    <span class="website-nav__span website-nav__cart">Корзина</span>
                                    <a href="<?php echo esc_url( wc_get_cart_url()) ?>" class="basket-btn basket-btn_fixed-xs display-none">
                                        <span class="basket-btn__counter"><?php echo sprintf($woocommerce->cart->cart_contents_count); ?></span>
                                    </a>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="container_catalog">
                    <div class="catalog_blocks">
                        <div class="catalog_block_name">ВСЕ НОЖИ</div>
                            <?php wp_nav_menu( [
                            	'theme_location'  => 'megamenu_column1',
                            	'container'       => null,
                            	'menu_class'      => 'catalog_block_name',
                            	'menu_id'         => '',
                            	'echo'            => true,
                            	'fallback_cb'     => 'wp_page_menu',
                            	'before'          => '',
                            	'after'           => '',
                            	'link_before'     => '',
                            	'link_after'      => '',
                            	'items_wrap'      => '<ul id="%1$s" class="header_menu_links %2$s">%3$s</ul>',
                            	'depth'           => 0,
                            	'walker'          => ''
                            ] );
                             ?>
                    </div>
                    <div class="catalog_blocks">
                        <div class="catalog_block_name">НОЖИ ПО МАРКЕ СТАЛИ</div>
                            <?php wp_nav_menu( [
                            	'theme_location'  => 'megamenu_column2',
                            	'container'       => null,
                            	'menu_class'      => 'catalog_block_name',
                            	'menu_id'         => '',
                            	'echo'            => true,
                            	'fallback_cb'     => 'wp_page_menu',
                            	'before'          => '',
                            	'after'           => '',
                            	'link_before'     => '',
                            	'link_after'      => '',
                            	'items_wrap'      => '<ul id="%1$s" class="header_menu_links %2$s">%3$s</ul>',
                            	'depth'           => 0,
                            	'walker'          => ''
                            ] );
                             ?>
                    </div>
                    <div class="catalog_blocks">
                        <div class="catalog_block_name">БРЕНДЫ</div>
                            <?php wp_nav_menu( [
                            	'theme_location'  => 'megamenu_column3',
                            	'container'       => null,
                            	'menu_class'      => 'catalog_block_name',
                            	'menu_id'         => '',
                            	'echo'            => true,
                            	'fallback_cb'     => 'wp_page_menu',
                            	'before'          => '',
                            	'after'           => '',
                            	'link_before'     => '',
                            	'link_after'      => '',
                            	'items_wrap'      => '<ul id="%1$s" class="header_menu_links %2$s">%3$s</ul>',
                            	'depth'           => 0,
                            	'walker'          => ''
                            ] );?>
                    </div>
                </div>
            </div>
            <div class="header_mobile_menu_container">
                <div class="mobile_menu_contacts_block">
                    <div class="location_closed_btn_block">
                        <div class="location">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/location.png">
                            <span>
                                <?php the_field('header_adress', 'options'); ?>
                            </span>
                        </div>
                        <button class="close_btn">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/close.png" alt="close btn">
                        </button>
                    </div>
                    <a href="tel:88001234567" class="phone"><?php the_field('header_tel', 'options'); ?></a>
                    <div class="mobile_block_social">
                        <span><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/vk.png" alt="vk icon"></a></span>
                        <span><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/youtube.png" alt="youtube icon"></a></span>
                    </div>
                </div>
                <nav class="mobile_nav">
                    <ul>
                        <li class="btn_catalog_mobile">
                            <span>Каталог</span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/chevron dovn.png" alt="chevron icon">
                        </li>
                        <!-- <div class="sub_catalog_title_link all_knives_catalog">ВСЕ НОЖИ</div> -->
                        <?php wp_nav_menu( [
                            'theme_location'  => 'megamenu_column1',
                            'container'       => null,
                            'menu_class'      => 'catalog_block_name_first',
                            'menu_id'         => '',

                            'echo'            => true,
                            'fallback_cb'     => 'wp_page_menu',
                            'before'          => '',
                            'after'           => '',
                            'link_before'     => '',
                            'link_after'      => '',
                            'items_wrap'      => '<ul id="%1$s" class="all_subcatalog %2$s">%3$s</ul>',
                            'add_li_class'  => 'all_subcatalog',
                            'depth'           => 0,
                            'walker'          => ''
                        ] );
                        ?>
                    <div class="catalog_blocks">
                        <div class="catalog_block_name drop_menu_marka_stali"><span>НОЖИ ПО МАРКЕ СТАЛИ</span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/chevron_down_silver.png" alt="chevron icon"></div>
                            <?php wp_nav_menu( [
                            	'theme_location'  => 'megamenu_column2',
                            	'container'       => null,
                            	'menu_class'      => 'catalog_block_name_second',
                            	'menu_id'         => '',
                            	'echo'            => true,
                            	'fallback_cb'     => 'wp_page_menu',
                            	'before'          => '',
                            	'after'           => '',
                            	'link_before'     => '',
                            	'link_after'      => '',
                            	'items_wrap'      => '<ul id="%1$s" class="header_menu_links %2$s">%3$s</ul>',
                            	'depth'           => 0,
                            	'walker'          => ''
                            ] );
                             ?>
                        </div>

                    <div class="catalog_blocks">
                        <div class="catalog_block_name drop_menu_brands"><span>БРЕНДЫ</span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/chevron_down_silver.png" alt="chevron icon"></div>
                            <?php wp_nav_menu( [
                            	'theme_location'  => 'megamenu_column3',
                            	'container'       => null,
                            	'menu_class'      => 'catalog_block_name_third',
                            	'menu_id'         => '',
                            	'echo'            => true,
                            	'fallback_cb'     => 'wp_page_menu',
                            	'before'          => '',
                            	'after'           => '',
                            	'link_before'     => '',
                            	'link_after'      => '',
                            	'items_wrap'      => '<ul id="%1$s" class="header_menu_links %2$s">%3$s</ul>',
                            	'depth'           => 0,
                            	'walker'          => ''
                            ] );?>
                    </div>
<!--                        <ul class="sub_catalog">-->
<!--                                <li class="all_subcatalog"><a href="#">Акции</a></li>-->
<!--                                <li class="all_subcatalog"><a href="#">Новинки</a></li>-->
<!--                                <li class="all_subcatalog"><a href="#">Хиты продаж</a></li>-->
<!--                                <li class="all_subcatalog"><a href="#">Военные ножи</a></li>-->
<!--                                <li class="all_subcatalog"><a href="#">Кухонные ножи</a></li>-->
<!--                                <li class="all_subcatalog"><a href="#">Мачете кукри</a></li>-->
<!--                                <li class="all_subcatalog"><a href="#">Ножи для выживания</a></li>-->
<!--                                <li class="all_subcatalog"><a href="#">Охотничьи ножи</a></li>-->
<!--                                <li class="all_subcatalog"><a href="#">Спортивные ножи</a></li>-->
<!--                                <li class="all_subcatalog"><a href="#">Товары для кемпинга</a></li>-->
<!--                                <li class="all_subcatalog"><a href="#">Топорики</a></li>-->
<!--                                <li class="all_subcatalog"><a href="#">Туристические ножи</a></li>-->
<!--                            <li class="sub_catalog_title_link">НОЖИ СОБСТВЕННОГО ПРОИЗВОДСТВА</li>-->
<!--                            <li class="sub_catalog_title_link marka_knives_catalog">НОЖИ ПО МАРКЕ СТАЛИ <img src="img/chevron down silver.png" alt=""></li>-->
<!--                                <ul class="marka_subcatalog_wrapper">-->
<!--                                    <li class="marka_subcatalog"><a href="#">Из дамасской стали</a></li>-->
<!--                                    <li class="marka_subcatalog"><a href="#">Из кованной стали 95х18</a></li>-->
<!--                                    <li class="marka_subcatalog"><a href="#">Из кованной стали 9хс</a></li>-->
<!--                                    <li class="marka_subcatalog"><a href="#">Из кованной стали XBS</a></li>-->
<!--                                </ul>-->
<!--                            <li class="sub_catalog_title_link">БРЕНДЫ <img src="img/chevron down silver.png" alt=""></li>-->
<!--                        </ul>-->
                        <li class="mobile_link"><a href="#">Ножи под заказ</a></li>
                        <li class="mobile_link"><a href="#">О мастерской</a></li>
                        <li class="mobile_link"><a href="#">Оплата</a></li>
                        <li class="mobile_link"><a href="#">Доставка</a></li>
                        <li class="mobile_link"><a href="#">Контакты</a></li>
                    </ul>
                </nav>
            </div>
        </header>
    <script>
        jQuery('.header_btn_menu').click(function(){
            // jQuery('.header_mobile_menu_container').css('display','block');
        })
    </script>