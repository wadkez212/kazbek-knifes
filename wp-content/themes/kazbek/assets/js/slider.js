
var $ = jQuery.noConflict();

$(document).ready(function() {
    //slider main
    $('.slider_wrapper_main').slick({
        dots: true,
        prevArrow: '<button class="slider_main_btn slider_main_btn_prev"><img src="http://dentbuzb.beget.tech/wp-content/uploads/2022/06/next.svg"></button>',    
        nextArrow: '<button class="slider_main_btn slider_main_btn_next"><img src="http://dentbuzb.beget.tech/wp-content/uploads/2022/06/prev.svg"></button>',
        responsive: [
            {
            breakpoint: 751,
            settings: {
                arrows: false
            }
            }
        ]
    });
});
