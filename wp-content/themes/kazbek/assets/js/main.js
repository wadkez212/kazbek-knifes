var $ = jQuery.noConflict();

//скрыть фильтр цены в каталоге
$(document).ready(function(){
    $(".wpf_form_price_filter").css("display", "none");
});


document.addEventListener('DOMContentLoaded', function() {

    //Отключаем класс desctop у тега main для того чтобы отключить hover эффекты на мобильной версии сайта
    switchHover();

    //При клике на кнопку каталога появляется каталог
    catalogShow();

    //Мобильное меню
    mobileMenu();

    //появления подкаталога
    showSubCatalog();

    //задаем ширину контейнера слайдера новинок
    widthSliderNew();

    //Слайдер новинок на главной
    sliderNewMain();

    //Слайдер хиты продаж на главной
    sliderHitMain();

    //Слайдер акции на главной
    sliderActionMain();
});

//Отключаем класс desctop у тега main для того чтобы отключить hover эффекты на мобильной версии сайта
function switchHover() {
    let width = window.innerWidth,
        main = document.querySelector('.main');
    if(width < 780) {
       // main.classList.remove('desctop');
    }
}

//При клике на кнопку каталога появляется каталог
function catalogShow() {
    let btn = document.querySelector('.header_btn_catalog'),
        container = document.querySelector('.container_catalog');
    
        btn.addEventListener('click', function() {
            container.classList.toggle('container_catalog_active');
        });
}

//Мобильное меню
function mobileMenu() {
    let btn = document.querySelector('.header_btn_menu'),
        container = document.querySelector('.header_mobile_menu_container'),
        btnClose = document.querySelector('.mobile_menu_contacts_block .close_btn');
    
    btn.addEventListener('click', function() {
        container.classList.add('header_mobile_menu_container_active');
    });

    btnClose.addEventListener('click', function() {
        container.classList.remove('header_mobile_menu_container_active');
    });
}

//появления подкаталога
function showSubCatalog() {
   let btn = document.querySelector('.marka_knives_catalog'),
       container = document.querySelector('.marka_subcatalog_wrapper');
    
    btn.addEventListener('click', function() {
        container.classList.toggle('marka_subcatalog_wrapper_active');
    });
    
}

// Появление кол-ва товаров рядом с корзиной в header
//Проверка при запууске сайта, тк товар может быть в куки файлах
$(document).ready(function(){
    
    $cart_count = $('.basket-btn__counter').html();
    
    if($cart_count > 0) {
        $(".basket-btn").removeClass("display-none");
    }
});

$(".catalog-gallery__btn").on("click", function(){
    $(".basket-btn").removeClass("display-none");
});

localStorage.clear();


 function add_compare(e){
     if(jQuery(e).hasClass('compare')){
         var id = jQuery(e).attr('data')
         jQuery(e).removeClass('compare');
         jQuery.ajax({
             url: window.wp_data.ajax_url,
             data: {
                 action : 'remove_compare',
                 product_id: id
             },
             success: function (response) {
                 var count = localStorage.getItem('count');
                 var now = count - 1;
                 localStorage.setItem('count', now)
                 console.log('AJAX response : ',response);
             }
         })
     }else{
         var id = jQuery(e).attr('data')
         jQuery(e).addClass('compare');
         jQuery.ajax({
             url: window.wp_data.ajax_url,
             data: {
                 action : 'add_compare',
                 product_id: id
             },
             success: function (response) {
                jQuery('.basket-btn').removeClass('ring');
                var count =  Number(localStorage.getItem('count'));
                var now = count + 1;
                 localStorage.setItem('count', now)
                 jQuery('.compare-btn__counter').html(localStorage.getItem('count'));
                 console.log('AJAX response : ',response);
             }
         })
     }
}


//задаем ширину контейнера слайдера новинок
function widthSliderNew() {
    let blocks = document.querySelectorAll('.promo_new_carts_slider_container ul.products  .catalog-gallery__item'),
        count = 0,
        width = 0,
        container = document.querySelector('.new_main ul.products');

    for(let i = 0; i < blocks.length; i++) {
        count = blocks.length;
        width = count * 300;
    }
    
    console.log("Ширина:" + width);
    container.style.width = width + 'px';
    //container.style.width = "1200px";
    
}

//Слайдер новинок на главной
function sliderNewMain() {
    const slide = document.querySelectorAll('.promo_new_carts_slider_container ul.products .catalog-gallery__item');
    const sliderLine = document.querySelector('ul.products');
    let count = 0;
    let width;


    function init() {
        width = document.querySelector('.promo_new_carts_slider_container ul.products .catalog-gallery__item').offsetWidth + 20;
    }

    init();
    
    console.log("Кол-во слайдов:" + slide.length);
    

    document.querySelector('.new_main_slider_btn_next').addEventListener('click', function() {
        count++;
        if(count >= slide.length - 4) {
            count = 0;
        }
        rollSlider();
    });

    document.querySelector('.new_main_slider_btn_prev').addEventListener('click', function() {
        count--;
        if(count < 0) {
            count = slide.length - 4;
        }
        rollSlider();
    })

    function rollSlider() {
        sliderLine.style.transform = 'translate(-'+ count * width +'px)';
    }
    
    // document.querySelector('.new_main_slider_btn_next').addEventListener("touchstart", (event) => {
    //      count++;
    //     if(count >= slide.length - 4) {
    //         count = 0;
    //     }
    //     rollSlider();
    //     console.log("Вы приложили палец к элементу");
    // });

}







//Слайдер хиты продаж на главной
function sliderHitMain() {
    const slide = document.querySelectorAll('.promo_hit_carts_slider_container .block');
    const sliderLine = document.querySelector('.promo_hit_carts_slider_container');
    let count = 0;
    let width;


    function init() {
        width = document.querySelector('.promo_hit_carts_slider_container .block').offsetWidth + 20;
    }

    init();

    document.querySelector('.hit_main_slider_btn_next').addEventListener('click', function() {
        count++;
        if(count >= slide.length - 3) {
            count = 0;
        }
        rollSlider();
    });

    document.querySelector('.hit_main_slider_btn_prev').addEventListener('click', function() {
        count--;
        if(count < 0) {
            count = slide.length - 4;
        }
        rollSlider();
    })

    function rollSlider() {
        sliderLine.style.transform = 'translate(-'+ count * width +'px)';
    }
}

//Слайдер акции на главной
function sliderActionMain() {
    const slide = document.querySelectorAll('.promo_action_carts_slider_container .block');
    const sliderLine = document.querySelector('.promo_action_carts_slider_container');
    let count = 0;
    let width;


    function init() {
        width = document.querySelector('.promo_action_carts_slider_container .block').offsetWidth + 20;
    }

    init();

    document.querySelector('.action_main_slider_btn_next').addEventListener('click', function() {
        count++;
        if(count >= slide.length - 3) {
            count = 0;
        }
        rollSlider();
    });

    document.querySelector('.action_main_slider_btn_prev').addEventListener('click', function() {
        count--;
        if(count < 0) {
            count = slide.length - 4;
        }
        rollSlider();
    })

    function rollSlider() {
        sliderLine.style.transform = 'translate(-'+ count * width +'px)';
    }
}




// МОБИЛЬНОЕ МЕНЮ (списки выведены подряд!!!!)
 var $ = jQuery.noConflict();

 $(document).ready(function() {
     $(".btn_catalog_mobile").on("click", function () {

         if (!$('.catalog_block_name_first').hasClass('active')) {
             $('.catalog_block_name_first').addClass('active');
             $(".btn_catalog_mobile img").css("transform", "rotate(180deg)");
         } else {
             $('.catalog_block_name_first').removeClass('active');
             $(".btn_catalog_mobile img").css("transform", "rotate(360deg)");
         }


     });

     // Вывести выпадающие меню, по умолчанию им добавлен класс вначале файла, который их скрывает, тк по другому они не скрываются потому что #menu-mega-menu-col2-1 хардкодом прописано display block
     $(".drop_menu_marka_stali").on("click", function () {

         if (!$('.catalog_block_name_second').hasClass('active')) {
             $('.catalog_block_name_second').addClass('active');
             $(".drop_menu_marka_stali img").css("transform", "rotate(180deg)");
         } else {
             $('.catalog_block_name_second').removeClass('active');
             $(".drop_menu_marka_stali img").css("transform", "rotate(360deg)");
         }
     });



     $(document).on("click", '.drop_menu_brands' ,function () {
         if (!$(".catalog_block_name_third").hasClass('active')) {
             $('.catalog_block_name_third').addClass('active');
             $(".drop_menu_brands img").css("transform", "rotate(180deg)");
         } else {
             $('.catalog_block_name_third').removeClass('active');
             $(".drop_menu_brands img").css("transform", "rotate(360deg)");
         }
     });
 });