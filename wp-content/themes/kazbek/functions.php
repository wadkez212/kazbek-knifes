<?php
/**
 * kazbek functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package kazbek
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
 
function kazbek_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on kazbek, use a find and replace
		* to change 'kazbek' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'kazbek', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'top', 'Main menu' ),
			'menu-2' => esc_html__( 'footer', 'Footer menu' ),
			'megamenu_column1' => esc_html__( 'Мегаменю колонка1', 'Mega menu column1' ),
			'megamenu_column2' => esc_html__( 'Мегаменю колонка2', 'Mega menu column2' ),
			'megamenu_column3' => esc_html__( 'Мегаменю колонка3', 'Mega menu column3' )
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'kazbek_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'kazbek_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function kazbek_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'kazbek_content_width', 640 );
}
add_action( 'after_setup_theme', 'kazbek_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function kazbek_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar Price', 'kazbek' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'kazbek' ),
			'before_widget' => '<section id="%1$s" class="widget price %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	
		register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar_Left', 'kazbek' ),
			'id'            => 'sidebar-left',
			'description'   => esc_html__( 'Add widgets here.', 'kazbek' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'kazbek_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function kazbek_scripts() {
    //wp_enqueue_style( 'kazbek-reset-style', get_stylesheet_uri() . "/assets/css/reset.css");
	wp_enqueue_style( 'kazbek-slick-theme', home_url() . '/wp-content/themes/kazbek/assets/css/slick-theme.css');
	wp_enqueue_style( 'kazbek-slick', home_url() . '/wp-content/themes/kazbek/assets/css/slick.css');
	wp_enqueue_style( 'kazbek-bootstrap', home_url() . '/wp-content/themes/kazbek/assets/css/bootstrap-reboot.min.css');
	
	wp_enqueue_style( 'kazbek-owl_carousel1', get_template_directory_uri() . "/owl.carousel.min.css");
	wp_enqueue_style( 'kazbek-owl_carousel2', get_template_directory_uri() . "/owl.theme.default.min.css");


	/*wp_enqueue_style( 'kazbek-style-header', home_url() . '/wp-content/themes/kazbek/assets/css/header.css');*/
	/*wp_enqueue_style( 'kazbek-style-catalog', home_url() . '/wp-content/themes/kazbek/assets/css/catalog.css');*/
	/*wp_enqueue_style( 'kazbek-style-cart', home_url() . '/wp-content/themes/kazbek/assets/css/cart.css'); */
	/*wp_enqueue_style( 'kazbek-style-single-product', home_url() . '/wp-content/themes/kazbek/assets/css/single-product.css');*/
	wp_enqueue_style( 'kazbek-style', home_url() . '/wp-content/themes/kazbek/assets/css/style.css');
    
	

    wp_enqueue_script( 'jquery' );
    	//wp_enqueue_script( 'kazbek-jquery', get_template_directory_uri() . '/assets/js/jq.js', array(), '1.0', true );
    	
    wp_enqueue_script( 'kazbek-owl_carousel_js', get_template_directory_uri() . '/owl.carousel.min.js', array(), '1.0', true );

	wp_enqueue_script( 'kazbek-mainjs', get_template_directory_uri() . '/assets/js/main.js', array(), '1.0', true );
	wp_enqueue_script( 'kazbek-slick', get_template_directory_uri() . '/assets/js/slick.min.js', array(), '1.0', true );
	wp_enqueue_script( 'kazbek-slider', get_template_directory_uri() . '/assets/js/slider.js', array(), '1.0', true );
	wp_enqueue_script( 'kazbek-catalog', get_template_directory_uri() . '/assets/js/catalog_page.js', array(), '1.0', true );
	wp_enqueue_script( 'kazbek-cart', get_template_directory_uri() . '/assets/js/feedback-form.js', array(), '1.0', true );
	// wp_enqueue_script( 'kazbek-compare', get_template_directory_uri() . '/assets/js/compare.js', array(), '1.0', true );


	// wp_style_add_data( 'kazbek-style', 'rtl', 'replace' );

	// wp_enqueue_script( 'kazbek-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'kazbek_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

