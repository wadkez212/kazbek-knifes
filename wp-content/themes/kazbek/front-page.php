<?php
/**
 * Theme Name: kazbek
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package kazbek
 */
session_start();
get_header();
?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/mobile-detect/1.4.4/mobile-detect.min.js"></script>
<script>
    let detect = new MobileDetect(window.navigator.userAgent)
</script>
<link
  rel="stylesheet"
  href="https://unpkg.com/swiper@8/swiper-bundle.min.css"
/>

<script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>

	<main id="primary" class="site-main">
 <section class="slider_main">
            <div class="slider_wrapper_main">
                <div class="slide slide1" id="slide1" style="background-image: url(<?php the_field('img_slide1'); ?>);">
                    <div class="containers">
                        <div class="slide_container">
                            <div class="slide_contant_block">
                                <h1 class="slide_title"><?php the_field('slider_title1'); ?></h1>
                                <h3 class="slide_subtitle"><?php the_field('slider_subtitle1'); ?></h3>
                                <button class="slide_btn_link"><a href="#"><?php the_field('slider_button1'); ?></a></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide" id="slide2" style="background-image: url(<?php the_field('img_slide2'); ?>);">
                    <div class="containers">
                        <div class="slide_container">
                            <div class="slide_contant_block">
                                <h1 class="slide_title"><?php the_field('slider_title2'); ?></h1>
                                <h3 class="slide_subtitle"><?php the_field('slider_subtitle2'); ?></h3>
                                <button class="slide_btn_link"><a href="#"><?php the_field('slider_button2'); ?></a></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide" id="slide3" style="background-image: url(<?php the_field('img_slide3'); ?>);">
                    <div class="containers">
                        <div class="slide_container">
                            <div class="slide_contant_block">
                                <h1 class="slide_title"><?php the_field('slider_title3'); ?></h1>
                                <h3 class="slide_subtitle"><?php the_field('slider_subtitle3'); ?></h3>
                                <button class="slide_btn_link"><a href="#"><?php the_field('slider_button3'); ?></a></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="about_main">
            <div class="containers">
                <h1 class="title">Магазин ножей сообственого изготавления “Кazbek”</h1>
            
                <div class="about_main_desctop_container">
                    <div class="about_main_content_block containers">
                        <img src="<?php the_field('about_img2'); ?>" alt="">
                        <ul class="about_main_list">
                            <li><?php the_field('about_li1'); ?></li>
                            <li><?php the_field('about_li2'); ?></li>
                            <li><?php the_field('about_li3'); ?></li>
                            <li><?php the_field('about_li4'); ?></li>
                        </ul>
                    </div>
                    <div class="block_img_and_mobile_content">
                         <img src="<?php the_field('about_img3'); ?>" class="about_main_img2" alt="">
                    </div>
                    <img style="height: 550px;" src="<?php the_field('about_img1'); ?>" alt="">
                </div>
            </div>
            <div class="about_main_tablet_container">
                <div class="about_main_tablet_block1">
                        <img src="<?php the_field('about_img2'); ?>" alt="">
                    <img style="height: 350px;" src="<?php the_field('about_img1'); ?>" alt="">
                </div>
                <div class="about_main_tablet_block2">
                         <img src="<?php the_field('about_img3'); ?>" class="about_main_img2" alt="">
                    <ul class="about_main_list">
                        <li>Сообственое производство</li>
                        <li>Любим свое дело</li>
                        <li>Чтим традиции изготовления клинков</li>
                        <li>Доставляем по всей россии</li>
                    </ul>
                </div>
            </div>
            <div class="about_main_mobile_container">
                <div class="block">
                   <div class="containers">
                        <ul class="about_main_list">
                            <li>Сообственое производство</li>
                            <li>Любим свое дело</li>
                            <li>Чтим традиции изготовления клинков</li>
                            <li>Доставляем по всей россии</li>
                        </ul>
                   </div>
                   <div class="block_img">
                        <img src="<?php the_field('about_img2'); ?>" alt="">
                        <img style="height: 250px;" src="<?php the_field('about_img1'); ?>" alt="">

                   </div>
                </div>
                <img src="<?php the_field('about_img3'); ?>" id="about_mobile_img">
            </div>
        </section>

         <section class="new_main">

            <div class="containers">
                <div class="new_main_header_block">
                    <div class="new_main_slider_btn_container">
                        <h3 class="promo_cart_title">Новинки</h3>
                     </div>
                    <div class="new_main_slider_btn">   
                        <button class="new_main_slider_btn new_main_slider_btn_prev"><img src="<?php the_field('switch_arrow_left'); ?>" alt=""></button>
                        <button class="new_main_slider_btn new_main_slider_btn_next"><img src="<?php the_field('switch_arrow_right'); ?>" alt=""></button>
                    </div>  
                </div>
                <div class="swiper novinki">
                    <div class="swiper-wrapper">
                    <?php echo do_shortcode('[products limit="16" category="novinki" ]'); ?>
                    </div>
                </div>
                </div>
            </div>
        </section>

        <section class="hits_main promo_main">

            <div class="containers">
                <div class="hits_main_header_block">
                    <h3 class="promo_cart_title">Хиты продаж</h3>
                    <div class="new_main_slider_btn">   
                        <div class="hits_button_prev"><img src="<?php the_field('switch_arrow_left'); ?>" alt=""></div>
                        <div class="hits_button_next"><img src="<?php the_field('switch_arrow_right'); ?>" alt=""></div>
                    </div>  
                </div>

                <div class="swiper hits">
                    <div class="swiper-wrapper">
                        <?php echo do_shortcode('[products limit="16" columns="4" category="hits" ]'); ?>
                    </div>
                </div>


            </div>

        </section>

        <section class="action_main">

            <div class="containers">

                <div class="hits_main_header_block">
                    <h3 class="promo_cart_title">Акции</h3>
                    <div class="new_main_slider_btn">   
                        <div class="action_button_prev"><img src="<?php the_field('switch_arrow_left'); ?>" alt=""></div>
                        <div class="action_button_next"><img src="<?php the_field('switch_arrow_right'); ?>" alt=""></div>
                    </div>  
                </div>

                <div class="swiper action">
                    <div class="swiper-wrapper">
                        <?php echo do_shortcode('[products limit="16" columns="4" category="promo" ]'); ?>
                    </div>
                </div>

            </div>

        </section>

        <section class="brends_main">

            <div class="containers">

                <h1 class="title"><?php the_field('brands_title'); ?></h1>

                <div class="brends_main_container">
                <!-- <button class="brends_btn_lin"><a href="#">Benchmade</a></button> -->
                    <?php
                        // проверяем есть ли в повторителе данные
                        if( have_rows('block_brands') ):
                         	// перебираем данные
                            while ( have_rows('block_brands') ) : the_row();
                                // отображаем вложенные поля
                                echo "<button class='brends_btn_lin'><a href='the_sub_field('link_brands')'>";
                                the_sub_field('name_brands');
                                echo "</a></button>";
                            endwhile;
                        else :
                            // вложенных полей не найдено
                        endif;
                    ?>

                </div>

            </div>

        </section>

        <section class="catigories_main">
            <div class="main-categories center">
                <marquee behavior="" direction="">
                    <?php
                        // проверяем есть ли в повторителе данные
                        if( have_rows('block_categories') ):
                         	// перебираем данные
                            while ( have_rows('block_categories') ) : the_row();
                                // отображаем вложенные поля
                                the_sub_field('name_categories');
                            endwhile;
                        else :
                            // вложенных полей не найдено
                        endif;
                    ?>
                </marquee>
            </div>
        </section>

        <section class="about1_main" style="background-image: url(<?php the_field('about_background'); ?>);">
            <div class="containers">
                <div class="about1_main_container">
                    <div class="block">
                        <img src="<?php the_field('about_company_img1'); ?>" alt="">
                        <img src="<?php the_field('about_company_img2'); ?>" alt="">
                    </div>
                    <div class="block">
                        <div class="text_block">
                            <h2 class="about_title"><?php the_field('about_company_title'); ?></h2>
                            <p>
                                <?php the_field('about_company_text'); ?>
                            </p>
                        </div>
                        <div class="about1_bg_block"></div>
                    </div>
                </div>
            </div>
        </section>

	</main><!-- #main -->
    <script>
        jQuery( document ).ready(function() {
            var proUL = jQuery( ".swiper-slide" );
            if ( proUL.parent().is( "div" ) ) {
                jQuery( ".swiper-slide" ).unwrap();
            }
        });
    </script>
    <script>
        var count = 4;
        // if (detect.mobile()){
        //     count = 1;
        // }

        var ww = jQuery(window).width();
        if (ww <= 1100) count = 3;
        if (ww <= 768) count = 2;
  

        var swiper = new Swiper(".novinki", {
            navigation: {
                nextEl: ".new_main_slider_btn_next",
                prevEl: ".new_main_slider_btn_prev",
            },
            slidesPerView: count,
            spaceBetween: 30,
        });
        var swiper = new Swiper(".hits", {
            navigation: {
                nextEl: ".hits_button_next",
                prevEl: ".hits_button_prev",
            },
            slidesPerView: count,
            spaceBetween: 30,
        });
        var swiper = new Swiper(".action", {
            navigation: {
                nextEl: ".action_button_next",
                prevEl: ".action_button_prev",
            },
            slidesPerView: count,
            spaceBetween: 30,
        });

    //     $(window).resize(function () {
    //   var ww = $(window).width();
    //   if (ww >= 1700) swiper.params.slidesPerView = 7;
    //   if (ww <= 1700) swiper.params.slidesPerView = 7;
    //   if (ww <= 1560) swiper.params.slidesPerView = 6;
    //   if (ww <= 1400) swiper.params.slidesPerView = 5;
    //   if (ww <= 1060) swiper.params.slidesPerView = 4;
    //   if (ww <= 800) swiper.params.slidesPerView = 3;
    //   if (ww <= 560) swiper.params.slidesPerView = 2;
    //   if (ww <= 400) swiper.params.slidesPerView = 1;
    // });

    // $(window).trigger('resize');
    </script>
<style>
    .swiper {
        width: 100%;
        height: 100%;
    }

    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background: #fff;
        max-width: 280px;
        /* Center slide text vertically */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }
</style>
<?php
/*get_sidebar();*/
get_footer();