<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
                <div class="catalog__sorting-main">
                    <ul class="sorting-main__list">
                        <li class="sorting-main__item">
                            <button class="sorting-main__button sorting-main__button--price">Цена</button>
                            <form class="sorting-main__price display-none">
                        <?php 
                            /*the_widget( 'WC_Widget_Price_Filter', array('title' => 'Цена', 'count' => true, 'dropdown' => true) );*/
                            /* premmerce filters shortcode */
                           /*echo do_shortcode('[premmerce_filter style="custom" bg_color="#fff" add_border="on" border_color="#000" bold_title="on" title_appearance="uppercase" price_input_bg="#fff" price_input_text="#000" price_slider_range="#000" price_slider_handle="#000" checkbox_appearance="0" title_size="14" title_color="#000" terms_title_size="14" terms_title_color="#000" checkbox_color="#000" checkbox_border_color="#000"]');*/
                           /* фильтр по цене Thimify product filter В виджетах висит в Sidebar Price, чтобы корректно отображался
                           + ОПИСАЛ ВСЕ в archive_product.php*/
                           echo do_shortcode('[searchandfilter id="price_filter"]'); 
                        ?>
                       <!-- <button class="sorting-main__price-submit" type="submit">применить</button> -->
                            </form>
                        </li>
                        <li class="sorting-main__item">
                            <button class="sorting-main__button sorting-main__button--brand">Бренд</button>
                            <form class="sorting-main__brand display-none">
                                <div class="check__wrapper">
                                    <? echo do_shortcode('[searchandfilter id="brand_filter"]'); ?>
                                </div>
                                <!--<button class="sorting-main__brand--submit">Применить</button>-->
                            </form>
                        </li>
                        <li class="sorting-main__item">
                            <button class="sorting-main__button sorting-main__button--steel">Марка стали</button>
                            <form class="sorting-main__steel display-none">
                                    <? echo do_shortcode('[searchandfilter id="markastali_filter"]'); ?>
                            </form>
                        </li>
                        <li class="sorting-main__item">
                          
                                <form class="woocommerce-ordering" method="get">
                                	<select name="orderby" class="orderby" aria-label="<?php esc_attr_e( 'Shop order', 'woocommerce' ); ?>">
                                		<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
                                			<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
                                		<?php endforeach; ?>
                                	</select>
                                	<input type="hidden" name="paged" value="1" />
                                	<?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>
                                </form>
                         
                        </li>
                    </ul>
                </div>
