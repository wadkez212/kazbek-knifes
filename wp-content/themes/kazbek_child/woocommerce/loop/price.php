<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
?>




<!-- АТРИБУТЫ ТОВАРОВ в single_product.php ОТОБРАЖЕНИЕ в базовой версии content-product.php 
в нашей версии этот код прописан в файле child_kazbek -> content-product.php
-->
<div class="single-properties">
	<span class="name-propertires">Марка стали</span>
	<div></div>
	<?php global $product; echo "<span>". $product->get_attribute('Марка стали') ."</span>";?> 
</div>

<div class="single-properties">
	<span class="name-propertires">Общая длина</span>
	<div></div>
	<?php global $product; echo "<span>". $product->get_attribute('Общая длина') ."</span>";?> 
</div>

<div class="single-properties">
	<span class="name-propertires">Длина клинка</span>
	<div></div>
	<?php global $product; echo "<span>". $product->get_attribute('Длина клинка') ."</span>";?> 
</div>

<div class="single-properties">
	<span class="name-propertires">Ширина клинка</span>
	<div></div>
	<?php global $product; echo "<span>". $product->get_attribute('Ширина клинка') ."</span>";?> 
</div>

<div class="single-properties">
	<span class="name-propertires">Толщина обуха</span>
	<div></div>
	<?php global $product; echo "<span>". $product->get_attribute('Толщина обуха') ."</span>";?> 
</div>

<div class="single-properties">
	<span class="name-propertires">Твердость клинка</span>
	<div></div>
	<?php global $product; echo "<span>". $product->get_attribute('tverdost-klinka') ."</span>";?> 
</div>

<div class="single-properties">
	<span class="name-propertires">Длина рукояти</span>
	<div></div>
	<?php global $product; echo "<span>". $product->get_attribute('Длина рукояти') ."</span>";?> 
</div>

<div class="single-properties">
	<span class="name-propertires">Материал рукояти</span>
	<div></div>
	<?php global $product; echo "<span>". $product->get_attribute('material-rukayati') ."</span>";?> 
</div>





<?php if ( $price_html = $product->get_price_html() ) : ?>
	<span class="price"><?php echo $price_html; ?></span>
<?php endif; ?>
