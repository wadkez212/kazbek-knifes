<?php
/** ШАБЛОН ДЛЯ WOOCOMERCE SHOP ВЫВОД ТОВАРОВ СПРАВА ОТ САЙДБАРА
 * ПРИ ПОИСКЕ ПРОДУКТОВ 2 (ХЛЕБНЫЕ КРОШКИ И ТД)
 * Шаблон для woocommerce shop был page.php
 * Theme Name: kazbek
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package kazbek
 */

get_header();
?>
<!-- Фильтры прикреплены в файле loop->orderby!!! -->
	<main id="primary" class="site-main">

	        <div class="product__wrapper">
	            
                
            		<?php
                        do_action( 'woocommerce_before_main_content' );
                        
                        ?>
                        <header class="woocommerce-products-header">
                        	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                        		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
                        	<?php endif; ?>
                        
                        	<?php
                        	/**
                        	 * Hook: woocommerce_archive_description.
                        	 *
                        	 * @hooked woocommerce_taxonomy_archive_description - 10
                        	 * @hooked woocommerce_product_archive_description - 10
                        	 */
                        	do_action( 'woocommerce_archive_description' );
                        	?>
                        </header>
                        
                        <?php
                        if ( woocommerce_product_loop() ) {
                        	do_action( 'woocommerce_before_shop_loop' ); ?>
                        
                        <div class="catalog">
                            <div class="catalog-gallery__wrapper">
                                <ul class="catalog-gallery__list">
                        <?	woocommerce_product_loop_start(); ?>

                        
                        <?	if ( wc_get_loop_prop( 'total' ) ) {
                        		while ( have_posts() ) {
                        			the_post();
                        

                        			do_action( 'woocommerce_shop_loop' );
                        
                        			wc_get_template_part( 'content', 'product' );
                        		}
                        	}
                        
                        	woocommerce_product_loop_end();
                        ?>
                                </ul>
                            </div>
                        <?php
                        /* Пагинация */
                        	do_action( 'woocommerce_after_shop_loop' );
                        } else {
                        	do_action( 'woocommerce_no_products_found' );
                        }
                        
                        do_action( 'woocommerce_after_main_content' );
                        
                        /*Инфа только для фильтра по цене!*/
                        /* Вызов сайдбара внизу страницы - вызывает по умолчанию sidebar-1 (сайдбар в файле sidebar.php в активной теме)*/
                        /* Если убрать строчку, то сайдбар по фильnрам отдельно от каталого не будет отображаться тк плагин с ним связан!*/
                        /* Помещаем фильтр цены плагина Product Filters в sidebar-1, тк он связан с woocommerce, далее через шотркод вызываем каждый виджет внутри sidebar-1 по отдельности*/
                        do_action( 'woocommerce_sidebar' ); 



            		?>
                 </div>
    		</div>
 

	</main><!-- #main -->

<?php
//get_sidebar();
//get_footer();
