<?php


    if ( ! defined( 'ABSPATH' ) ) {
    	exit; // Exit if accessed directly
    }
    get_header( 'shop' ); ?>
    
    <div class="containers">
    	<?php
    
    		do_action( 'woocommerce_before_main_content' );
    	?>
    	
    	
    	
    	<?php while ( have_posts() ) : ?>
    		<?php the_post(); ?>
    <div>
            <?php wc_get_template_part( 'content', 'single-product' ); ?>


    </div>

    	<?php endwhile; // end of the loop. ?>
    
    
    	<?php
    		do_action( 'woocommerce_after_main_content' );
    	?>
    	
    	
    	
    	
    		<?php
    		/**
    		 * woocommerce_after_main_content hook.
    		 *
    		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
    		 */
    		do_action( 'woocommerce_after_main_content' );
    	?>
    </div>

	
