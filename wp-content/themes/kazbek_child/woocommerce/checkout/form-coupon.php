<?php
/**
Тег формы нужно оставить, иначе woocommerce не обработает заказ, тк для его обработки
нужны все данные, включая coupon-form
*/

defined( 'ABSPATH' ) || exit;

if ( ! wc_coupons_enabled() ) { // @codingStandardsIgnoreLine.
	return;
}

?>

<form class="checkout_coupon woocommerce-form-coupon" method="post" style="display:none">

</form>
