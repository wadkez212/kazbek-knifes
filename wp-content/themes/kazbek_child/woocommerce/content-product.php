<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
if(is_front_page()){
?>
<div class="swiper-slide">
    <div class="compare_button <?php
    
    ?>" data="<?php echo $product->id ?>" onclick="add_compare(this)">
    </div>
    <a href="<?php echo get_permalink() ?>">
    <li class="catalog-gallery__item" style="width:100%!important">
        <?php
        $cat_ids = $product->get_category_ids();
        foreach($cat_ids as $id){
            switch ($id) {
                case '27':
                    $class = 'Orange';
                    $text = 'New';
                    break;
                case '46':
                    $sale_price =  $product->get_sale_price();
                    $base_price =  $product->get_regular_price();
                    $price  =  $sale_price * 100 / $base_price;
                    $class = 'Blue';
                    $text =  round(100 - $price);
                    $text = '-'.$text.'%';
                    break;
                case '45':
                    $class = 'Red';
                    $text = 'Хиты';
                    break;
            }
        }
        ?>

        <div class="tag_slider <?php echo $class; ?>">
           <?php echo $text; ?>
        </div>
    <? do_action( 'woocommerce_before_shop_loop_item' ); ?>
    <!-- Изображение товара -->
    <div class="img_block" style="text-align: center">
        <?php	do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
    </div>
    <!-- Заголовок товара -->
    <h5 class="catalog-gallery__title"><?php do_action( 'woocommerce_shop_loop_item_title' ); ?></h5>
    <table class="catalog-gallery__description">
        <tr>
            <!-- Отображаем атрибуты, но в Товары -> Атрибуты мы их не регистрируем, тк они нужны только для верстки -->
            <td class="prop">Общая длина, мм</td>
            <td class="prop_value"><?php echo $product->get_attribute('total_length'); ?> </td>
        </tr>
        <tr>
            <td class="prop">Длина клинка, мм</td>
            <td class="prop_value"><?php echo $product->get_attribute('blade_length'); ?> </td>
        </tr>
        <tr>
            <td class="prop">Ширина клинка, мм</td>
            <td class="prop_value"><?php echo $product->get_attribute('blade_width'); ?> </td>
        </tr>
    </table>
    <!-- Цена -->
    <p class="catalog-gallery__price">
        <?php if ( $price_html = $product->get_price_html() ) : ?>
            <span class="price"><?php echo $price_html; ?></span>
        <?php endif; ?>
    </p>
    <!-- КНОПКА Добавить в корзину -->
    <button class="catalog-gallery__btn"><img class="cart_icon" src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/07/cart-icons.svg"><?php do_action( 'woocommerce_after_shop_loop_item' ); ?></button>
    </li>
    </a>
</div>
<?php }
elseif(is_single())
{
?>

    <div class="swiper-slide">
        <div class="compare_button <?php
        if(array_search($product->id ,$_SESSION['compare'])){
            echo 'compare';
        }
        ?>" data="<?php echo $product->id ?>" onclick="add_compare(this)">
        </div>
        <a href="<?php echo get_permalink() ?>">
            <li class="catalog-gallery__item" style="width:100%!important">
  
                <? do_action( 'woocommerce_before_shop_loop_item' ); ?>   
          
                <!-- Изображение товара -->
                <div class="img_block" style="text-align: center">
                    <?php	do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
                </div>
                <!-- Заголовок товара -->
                <h5 class="catalog-gallery__title"><?php do_action( 'woocommerce_shop_loop_item_title' ); ?></h5>
                <table class="catalog-gallery__description">
                    <tr>
                        <!-- Отображаем атрибуты, но в Товары -> Атрибуты мы их не регистрируем, тк они нужны только для верстки -->
                        <td class="prop">Общая длина, мм</td>
                        <td class="prop_value"><?php echo $product->get_attribute('total_length'); ?> </td>
                    </tr>
                    <tr>
                        <td class="prop">Длина клинка, мм</td>
                        <td class="prop_value"><?php echo $product->get_attribute('blade_length'); ?> </td>
                    </tr>
                    <tr>
                        <td class="prop">Ширина клинка, мм</td>
                        <td class="prop_value"><?php echo $product->get_attribute('blade_width'); ?> </td>
                    </tr>
                </table>
                <!-- Цена -->
                <p class="catalog-gallery__price">
                    <?php if ( $price_html = $product->get_price_html() ) : ?>
                        <span class="price"><?php echo $price_html; ?></span>
                    <?php endif; ?>
                </p>
                <!-- КНОПКА Добавить в корзину -->
                <button class="catalog-gallery__btn"><?php do_action( 'woocommerce_after_shop_loop_item' ); ?></button>
            </li>
        </a>
    </div>

    <?php
}else{
    ?>

    <li class="catalog-gallery__item">
        <div class="compare_button" data="<?php echo $product->id ?>" onclick="add_compare(this)">
        </div>
<!-- ЗНАЧОК СРАВНЕНИЯ (compare) прописан в style.css -->
                            <!--<img class="catalog__special-offer catalog__special-offer--new"
                                 src="img/catalog__special-offer--new.png" alt="new product">
                            </img> -->
                            <!-- чтобы переходил в карточку товара single-product -->
                            <? do_action( 'woocommerce_before_shop_loop_item' ); ?>
                            <!-- Изображение товара -->
                            <div class="img_block">
                            <?php	do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
                            </div>
                            <!-- Заголовок товара -->
                            <h5 class="catalog-gallery__title"><?php do_action( 'woocommerce_shop_loop_item_title' ); ?></h5>
                            <table class="catalog-gallery__description">
                                <tr>
                                    <!-- Отображаем атрибуты, но в Товары -> Атрибуты мы их не регистрируем, тк они нужны только для верстки -->
                                    <td class="prop">Общая длина, мм</td>
                                    <td class="prop_value"><?php echo $product->get_attribute('total_length'); ?> </td>
                                </tr>
                                <tr>
                                    <td class="prop">Длина клинка, мм</td>
                                    <td class="prop_value"><?php echo $product->get_attribute('blade_length'); ?> </td>
                                </tr>
                                <tr>
                                    <td class="prop">Ширина клинка, мм</td>
                                    <td class="prop_value"><?php echo $product->get_attribute('blade_width'); ?> </td>
                                </tr>
                            </table>
                            <!-- Цена -->
                            <p class="catalog-gallery__price">
                                <?php if ( $price_html = $product->get_price_html() ) : ?>
                                	<span class="price"><?php echo $price_html; ?></span>
                                <?php endif; ?>
                            </p>
                            <!-- КНОПКА Добавить в корзину --> 
                            <button class="catalog-gallery__btn"><?php do_action( 'woocommerce_after_shop_loop_item' ); ?></button>
                        </li>


    <?php
    }
?>




