<!-- ТАБЫ ПРОПИСАНЫ В functions.php -->
<!-- Атрибуты выведены в loop -> price.php -->

<link
        rel="stylesheet"
        href="https://unpkg.com/swiper@8/swiper-bundle.min.css"
/>

<script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>

<main id="primary" class="site-main">
<?php do_action( 'woocommerce_before_single_product' ); ?>
<section class="product-single">
    <div class="containers">
				<div class="title-product">
					<h1 class="title">
					    <?php
    					    global $product;
    					    echo $product->get_title();
                        ?>
					</h1>
				</div>

				<div class="single-product" style="display: flex">
					
					<div class="single_product_gallery">

						<div class="product_foto_block">
						    <!-- фото товара -->
						   <?php do_action( 'woocommerce_before_single_product_summary' ); ?>
						</div>
					</div>
					
                    <!-- Атрибуты выведены в loop -> price.php -->
					<div class="short_description">
					    <?php do_action( 'woocommerce_after_shop_loop_item_title', 'show_attributes', 20 ); ?>
                        <?php /*do_action( 'woocommerce_single_product_summary' );*/ ?>
                        
                        
                        
    <!--  Добавить в корзину Скопировал код из ../plugins/woocommerce/templates/single-product/add-to-cart/simple.php********************************************************************************************************* -->        
         <?php
                defined( 'ABSPATH' ) || exit;
                
                global $product;
                
                if ( ! $product->is_purchasable() ) {
                	return;
                }
                
                echo wc_get_stock_html( $product ); // WPCS: XSS ok.
                
                if ( $product->is_in_stock() ) : ?>
                
                	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
                
                	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
                		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
                		
                        <!-- кнопка "В корзину" add to cart и сравнения из верстки -->
                        <div class="block-single-cart">
                		    <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><img class="cart_icon" src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/07/cart-icons.svg"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
                		    <!-- <img class="single-calc" src="<?php echo get_site_url(); ?>/wp-content/uploads/2022/07/calc.png" alt="" width="32" height="32"> -->
							<div class="compare_button_single  <?php
								if(in_array((string)$product->id, $_SESSION['compare'])){
									echo 'compare';
								}
								?>" data="<?php echo $product->id ?>" onclick="add_compare(this)">
							</div>
                		</div>


                
                		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
                	</form>
                
                	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
                
                <?php endif; ?>
<!-- ********************************************************************************************************* -->    
						<div class="single-text">Если у вас появились вопросы позвоните нам <br> по телефону 8 (800) 123-45-67</div>
					</div> 
					
				</div>
			</section>



<!-- Похожие товары изменяем вывод на 4 товара в строку -->
            <?php
            add_filter( 'woocommerce_output_related_products_args', 'change_number_of_related_products' );
            
            function change_number_of_related_products( $args ) {
              $args['posts_per_page'] = 4; // количество выводимых похожих товаров
              $args['columns']        = 4;    // количесто столбцов
              return $args;
            }
            ?>
			<!-- Выводим похожие товары -->
		        <div>
		            <!-- встроенная функция woocommerce -->
                        <?php /*do_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs');*/ ?>
                    <div class="containers">
                        <div class="new_main_header_block">
                            <div class="new_main_slider_btn_container">
                                <h3 class="promo_cart_title">Похожие товары</h3>
                            </div>
                            <div class="new_main_slider_btn">
                                <button class="new_main_slider_btn new_related_slider_btn_prev"><img src="<?php get_site_url(); ?>/wp-content/themes/kazbek/assets/img/btn_cart_prev.png" alt=""></button>
                                <button class="new_main_slider_btn new_related_slider_btn_next"><img src="<?php get_site_url(); ?>/wp-content/themes/kazbek/assets/img/cart_btn_next.png" alt=""></button>
                            </div>
                            </div>
                        </div>

                        <div class="swiper related">
                            <div class="swiper-wrapper">
                                <?php echo do_shortcode('[related_products orderby="price" order="desc"]'); ?>

                                <?php /*do_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs');*/ ?>
                            </div>
                        </div>
                    </div>
			    </div>
			    
			    
			    
			<!-- Выводим Вы смотрели ранее (Создал шорткод в functions.php)
			там же поставил выводить кол-ва товаров в строку 4 -->
    <div class="containers">
    <div class="new_main_header_block">
        <div class="new_main_slider_btn_container">
            <h3 class="promo_cart_title">Вы уже смотрели</h3>
        </div>
        <div class="new_main_slider_btn">
            <button class="new_main_slider_btn new_main_slider_btn_prev"><img src="<?php get_site_url(); ?>/wp-content/themes/kazbek/assets/img/btn_cart_prev.png" alt=""></button>
            <button class="new_main_slider_btn new_main_slider_btn_next"><img src="<?php get_site_url(); ?>/wp-content/themes/kazbek/assets/img/cart_btn_next.png" alt=""></button>
        </div>
    </div>

    <div class="swiper single_watch">
        <div class="swiper-wrapper">
			    <?php echo do_shortcode('[recently_viewed_products]'); ?>
			</div>
    </div>
    </div>

    <script>
        jQuery( document ).ready(function() {
            var proUL = jQuery( ".swiper-slide" );
            if ( proUL.parent().is( "div" ) ) {
                jQuery( ".swiper-slide" ).unwrap();
            }
        });
    </script>
    <script>
        var count = 4;
        // if (detect.mobile()){
        //     count = 1;
        // }

        var ww = jQuery(window).width();
        if (ww <= 1100) count = 3;
        if (ww <= 768) count = 2;


        var swiper = new Swiper(".single_watch", {
            navigation: {
                nextEl: ".new_main_slider_btn_next",
                prevEl: ".new_main_slider_btn_prev",
            },
            slidesPerView: count,
            spaceBetween: 30,
        });
        var swiper = new Swiper(".related", {
            navigation: {
                nextEl: ".new_related_slider_btn_next",
                prevEl: ".new_related_slider_btn_prev",
            },
            slidesPerView: count,
            spaceBetween: 30,
        });


        //     $(window).resize(function () {
        //   var ww = $(window).width();
        //   if (ww >= 1700) swiper.params.slidesPerView = 7;
        //   if (ww <= 1700) swiper.params.slidesPerView = 7;
        //   if (ww <= 1560) swiper.params.slidesPerView = 6;
        //   if (ww <= 1400) swiper.params.slidesPerView = 5;
        //   if (ww <= 1060) swiper.params.slidesPerView = 4;
        //   if (ww <= 800) swiper.params.slidesPerView = 3;
        //   if (ww <= 560) swiper.params.slidesPerView = 2;
        //   if (ww <= 400) swiper.params.slidesPerView = 1;
        // });

        // $(window).trigger('resize');
    </script>
    <style>
        .swiper-wrapper h2:first-child{
            display:none;
        }
        .swiper {
            width: 100%;
            height: 100%;
        }

        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;
            max-width: 280px;
            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
    </style>
			
			</main>