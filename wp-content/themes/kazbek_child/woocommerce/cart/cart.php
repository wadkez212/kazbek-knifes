<?php defined( 'ABSPATH' ) || exit; 
do_action( 'woocommerce_before_cart' ); ?>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<main id="primary" class="site-main">
    <?php
    if(!wp_is_mobile()){
    ?>
<section class="responsive-version__wrapper">
    <div class="cart-page__containers">

    <form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
        <?php do_action( 'woocommerce_before_cart_table' ); ?>

        <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
            <colgroup>
                <col class="cart-page-sorting-bloc__items1">
                <col class="cart-page-sorting-bloc__items2">
                <col class="cart-page-sorting-bloc__items3">
                <col class="cart-page-sorting-bloc__items4">
                <col class="cart-page-sorting-bloc__items5">
            </colgroup>
            <tr>
                <td></td>
                <td class="cart-page-sorting-bloc">Название</td>
                <td class="cart-page-sorting-bloc">Кол-во</td>
                <td class="cart-page-sorting-bloc">Цена</td>
                <td></td>
            </tr>

            <?php do_action( 'woocommerce_before_cart_contents' ); ?>
            
			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
            
            <tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                <td class="a1">
                    <div class="compare_button <?php
                    if(in_array((string)$_product->id, $_SESSION['compare'])){
                        echo 'compare';
                    }
                    ?>"></div>
                <?php
						$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

						if ( ! $product_permalink ) {
							echo $thumbnail; // PHPCS: XSS ok.
						} else {
							printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
						}
						?>
                </td>
                <!-- Название товара -->
                <td class="a2">
                <div>
                <?php
                
						if ( ! $product_permalink ) {
                            echo "<h4 class='cart-page-text__title'>".
                                wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' )
                                . "</h4>";
						} else {
							echo "<h4 class='cart-page-text__title'>".
                            wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) )
                            . "</h4>";
						}

						do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

						// Meta data.
						echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

						// Backorder notification.
						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
						}
						?>
                    </div>
                </td>
                <td class="a3">
                <div class="cart-page-control-wrapper">
                                    <!--Сами хуки для кол-ва товара находятся в 
                                    woocommerce->templates->global->qunatity-input.php
                                    Изменяем их в funtcions.php-->
                                    
                                    <!-- кнопки + и - переопределены в functions.php -->
                                    <!-- <button class="plus">+</button> -->
                                    <p class="cart-page-control__quantity-product data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>'>
                                        <?php
                                            /* input корзины */
                    						if ( $_product->is_sold_individually() ) {
                    							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                    						} else {
                    							$product_quantity = woocommerce_quantity_input(
                    								array(
                    									'input_name'   => "cart[{$cart_item_key}][qty]",
                    									'input_value'  => $cart_item['quantity'],
                    									'max_value'    => $_product->get_max_purchase_quantity(),
                    									'min_value'    => '0',
                    									'product_name' => $_product->get_name(),
                    								),
                    								$_product,
                    								false
                    							);
                    						}
                    
                    						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
						                ?>
                                        </p>
                                    <!-- <button class="cart-control__btn cart-control__btn--minus">-</button> -->
                                </div>
                </td>

                <!-- Цена -->
                <td class="a4 product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>">
                    <div class="cart-page-price">

                        <?php
								echo "<p class='cart-page-price__paragraph'>". apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ) . "</p>";
							?>
                    </div>
                </td>
                <!-- Remove -->
                <td class="a5">
                    <div class="product-remove">
                        <!-- <button class="product-remove__btn"></button> -->
                        <?php
								echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
									'woocommerce_cart_item_remove_link',
									sprintf(
										'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
										esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
										esc_html__( 'Remove this item', 'woocommerce' ),
										esc_attr( $product_id ),
										esc_attr( $_product->get_sku() )
									),
									$cart_item_key
								);
							?>
                        <p class="display-none product-remove__hint">удалить товар из корзины</p>
                    </div>
                </td>
            </tr>
            <?php
				}
			}
			?>
        </table>
        
        <!-- Чтобы работал + и - в корзине -->
        <div  class="actions">
            <button type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

            <?php do_action( 'woocommerce_cart_actions' ); ?>

            <?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
        </div>
        <?php do_action( 'woocommerce_after_cart_table' ); ?>

    </form>		
    
    <!-- Checkout -->
    <section class="cart-all-in-all">
            <p class="cart-all-in-all__total">Итого:</p>
            <h4 class="cart-all-in-all__total-price">
                <?php echo WC()->cart->get_cart_subtotal(); ?>
            </h4>
            <button class="cart-all-in-all__order-btn" data-toggle="modal" data-target="#exampleModal">Оформить заказ</button>
        </section>


 
</section>
    <?php }else{ ?>
<!-- -----------------------------------------------------------end tablet version---------------------------------->
<!-- ---------------------------------------------------------------------mobile version ---------------------------------- -->
<section class="cart-page__mobile-version">
    <div class="cart-page__mobile-containers">

    <form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
        <?php do_action( 'woocommerce_before_cart_table' ); ?>


        <ul class="sorting-bloc">
            <li class="sorting-bloc__items">Название</li>
            <li class="sorting-bloc__items">Кол-во</li>
            <li class="sorting-bloc__items">Цена</li>
        </ul>

        <?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>

        <ul class="cart-page__list">
            <li class="cart-page__item">
                <div class="cart-page-product__wrapper">

                    <!-- картинка товара -->
                    <?php
                        $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                        if ( ! $product_permalink ) {
                            echo $thumbnail; // PHPCS: XSS ok.
                        } else {
                            printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
                        }
                        ?>
                </div>
                <!-- Название товара -->
                <div class="cart-page-product__mobile-wrapper">
                    <div class="cart-page-text">
                    <?php
                        if ( ! $product_permalink ) {
                            echo "<h4 class='cart-page-text__title'>" . wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' ) . "</h4>";
                        } else {
                            echo "<h4 class='cart-page-text__title'>" . wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) ) . "</h4>";
                        }
                        do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );
                        echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.
                    ?>                        
                    <!-- <p class="cart-page__product--available-order">Доступен под заказ</p> -->
                    </div>
                    <!-- кол-во товара -->
                    <div class="cart-page-control-wrapper">
                                       <!--Сами хуки для кол-ва товара находятся в 
                                    woocommerce->templates->global->qunatity-input.php
                                    Изменяем их в funtcions.php-->
                                    
                                    <!-- кнопки + и - переопределены в functions.php -->
                                    <!-- <button class="plus">+</button> -->
                                    <p class='cart-control__quantity-product data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>'>
                                        <?php
                                            /* input корзины */
                    						if ( $_product->is_sold_individually() ) {
                    							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                    						} else {
                    							$product_quantity = woocommerce_quantity_input(
                    								array(
                    									'input_name'   => "cart[{$cart_item_key}][qty]",
                    									'input_value'  => $cart_item['quantity'],
                    									'max_value'    => $_product->get_max_purchase_quantity(),
                    									'min_value'    => '0',
                    									'product_name' => $_product->get_name(),
                    								),
                    								$_product,
                    								false
                    							);
                    						}
                    
                    						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
						                ?>
                                        </p>
                    </div>

                    <!-- Цена товара -->
                    <div class="cart-page-page-price">
                        <p class="cart-page-price__paragraph">
                            <?php
                                echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
                            ?>
                        </p>
                        <!-- <p class="cart-page-price__sale">скидка 20%</p> -->
                    </div>
                </div>
                <div class="product-remove">
                    <!-- <button class="product-remove__btn"></button> -->
                    <?php
                        echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                            'woocommerce_cart_item_remove_link',
                            sprintf(
                                '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
                                esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                                esc_html__( 'Remove this item', 'woocommerce' ),
                                esc_attr( $product_id ),
                                esc_attr( $_product->get_sku() )
                            ),
                            $cart_item_key
                        );
                    ?>
                    <p class="display-none product-remove__hint">удалить товар из корзины</p>
                </div>
            </li>
        </ul>

        <?php
				}
			}
			?>   

                    <!-- Чтобы работал + и - в корзине -->
        <div  class="actions">
            <button type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

            <?php do_action( 'woocommerce_cart_actions' ); ?>

            <?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
        </div>
        <?php do_action( 'woocommerce_after_cart_table' ); ?>

    </form>		
    
    <!-- Checkout -->
    <section class="cart-all-in-all">
            <p class="cart-all-in-all__total">Итого:</p>
            <h4 class="cart-all-in-all__total-price">
                <?php echo WC()->cart->get_cart_subtotal(); ?>
            </h4>
            <button class="cart-all-in-all__order-btn" data-toggle="modal" data-target="#exampleModal">Оформить заказ</button>
        </section>
		</form>
    </div>
</section>
        <script>
jQuery('.plus').click(function(){

})
jQuery('.minus').click(function(){

})
        </script>
    <?php } ?>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div><p style="text-align:center" class="modal-title" id="exampleModalLabel">Пожалуйста, заполните поля и наш менеджер свяжется с вами для уточнения деталей заказа</p></div>
                <div class="modal-body">
                <!-- <h5 class="modal-title" id="exampleModalLabel">Детали оплаты</h5> -->

                    <?php echo do_shortcode('[woocommerce_checkout]'); ?>
                </div>
            </div>
        </div>
    </div>
<!-- ------------------------------------------------------------------end mobile version--------------------------------------  -->
<!-- <section class="feedback-form__wrapper display-none">
    <form class="feedback-form">
        <button class="feedback-form__btn-close"></button>
        <h5 class="feedback-form__title">Пожалуйста, заполните поля и наш менеджер
            свяжется с вами для уточнения деталей заказа</h5>
        <div class="feedback-form__name-wrapper">
            <label for="name" class="feedback-form__name-label">Имя</label>
            <input id="name" class="feedback-form__name-input" name="name" type="text">
        </div>
        <div class="feedback-form__phone-wrapper">
            <label for="phone" class="feedback-form__phone-label">Телефон</label>
            <input id="phone" class="feedback-form__phone-input" name="tel" type="tel"
                   placeholder="+7 (999) 999-99-99">
        </div>
        <div class="feedback-form__email-wrapper">
            <label for="email" class="feedback-form__email-label">Почта</label>
            <input id="email" class="feedback-form__email-input" name="email" type="email"
                   placeholder="email@mail.ru">
        </div>
        <div class="feedback-form__comment-wrapper">
            <label for="comment" class="feedback-form__comment-label">Комментарий</label>
            <textarea id="comment" class="feedback-form__comment-input" name="textComment"></textarea>
        </div>
        <div class="feedback-form__submit-wrapper">
            <button class="feedback-form__submit" type="submit">Отправить запрос</button>
            <p class="feedback-form__agreement">Нажимая кнопку "Отправить запрос", вы принимаете условия<a
                    href="#"> Оферты</a>
                и <a href="#">Политики Конфиденциальности</a></p>
        </div>
    

	</section> -->
</main>