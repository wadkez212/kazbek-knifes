<? /* Template Name: Шаблон корзины без сайдбара */ ?>
    <div class="product__wrapper">
        <div class="containers">
            <?php defined( 'ABSPATH' ) || exit; ?>





<main id="primary" class="site-main">
    <form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
    		<ul class="sorting-bloc">
                <li class="sorting-bloc__items">Название</li>
                <li class="sorting-bloc__items">Кол-во</li>
                <li class="sorting-bloc__items">Цена</li>
            </ul>
            
            
    
			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					
	
					<ul class="cart <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                        <li class="cart__item">
                            <div class="cart-product__wrapper">
                                <?php
						            $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

						            if ( ! $product_permalink ) {
							            echo $thumbnail; // PHPCS: XSS ok.
						            } else {
							            printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
						            }
						          ?>
                            </div>
                            
                            <div class="cart-product__mobile-wrapper">
                                <div class="cart-text">
                                    <h4 class="cart-text__title">
                                        <?php
                    						if ( ! $product_permalink ) {
                    							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
                    						} else {
                    							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
                    						}
                    						do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );
                    						echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.
						                ?>
                                    </h4>
                                    <!-- <p class="cart__product--available-order">Доступен под заказ</p> -->
                                </div>
                                
                                <div class="cart-control-wrapper">
                                    <!--Сами хуки для кол-ва товара находятся в 
                                    woocommerce->templates->global->qunatity-input.php
                                    Изменяем их в funtcions.php-->
                                    
                                    <!-- кнопки + и - переопределены в functions.php -->
                                    <!-- <button class="plus">+</button> -->
                                    <p class='cart-control__quantity-product data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>'>
                                        <?php
                                            /* input корзины */
                    						if ( $_product->is_sold_individually() ) {
                    							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                    						} else {
                    							$product_quantity = woocommerce_quantity_input(
                    								array(
                    									'input_name'   => "cart[{$cart_item_key}][qty]",
                    									'input_value'  => $cart_item['quantity'],
                    									'max_value'    => $_product->get_max_purchase_quantity(),
                    									'min_value'    => '0',
                    									'product_name' => $_product->get_name(),
                    								),
                    								$_product,
                    								false
                    							);
                    						}
                    
                    						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
						                ?>
                                        </p>
                                    <!-- <button class="cart-control__btn cart-control__btn--minus">-</button> -->
                                </div>
                                
                                <div class="cart-price">
                                    <p class="cart-price__paragraph">
                                        <?php
								            echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							            ?>
                                    </p>
                                    <p class="display-none cart-price__sale">скидка 20%</p>
                                </div>
                            </div>
                            
                            <div class="product-remove">
                                    <?php
        								echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
        									'woocommerce_cart_item_remove_link',
        									sprintf(
        										'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
        										esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
        										esc_html__( 'Remove this item', 'woocommerce' ),
        										esc_attr( $product_id ),
        										esc_attr( $_product->get_sku() )
        									),
        									$cart_item_key
        								);
							        ?>
                                <p class="display-none product-remove__hint">удалить товар из корзины</p>
                            </div>                          
                        </li>
					</ul>
					
					<?php
				}
			}
			?>   
	
			
			
			<div  class="actions">


					<button type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
			</div>
	</form>		
			
			        <section class="cart-all-in-all">
            <p class="cart-all-in-all__total">Итого:</p>
            <h4 class="cart-all-in-all__total-price">
                <?php echo WC()->cart->get_cart_subtotal(); ?>
            </h4>
            <button class="cart-all-in-all__order-btn">Оформить заказ</button>
        </section>
        <section class="feedback-form__wrapper display-none">
            <form class="feedback-form">
                <button class="feedback-form__btn-close"></button>
                <br>
                <?php echo do_shortcode('[woocommerce_checkout]'); ?>
            </form>
        </section>

</main>
        </div>
    </div>