<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package kazbek
 */

get_header();
?>

	<main id="primary" class="site-main">

		<section class="error-404 not-found">


			        <div class="alert">
            <picture class="alert__img">
                <source media="(max-width: 680px)" srcset="http://graftid4.beget.tech/wp-content/uploads/2022/07/404__mobile.png" width="298px">
                <img src="<?php echo get_site_url();?>/wp-content/uploads/2022/07/404__desktop.png" alt="alert img" width="378px">
            </picture>
            <section class="alert__text">
                <h1 class="alert__title">Мы не нашли эту страницу</h1>
                <p class="alert__paragraph">Возможно вы неправильно набрали адрес или такой
                    страницы не существует</p>
                <a class="alert__comeback-btn" href="/">Вернуться на главную</a>
            </section>
        </div>

		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();
