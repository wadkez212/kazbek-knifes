<?php get_header(); ?>

<main id="primary" class="site-main">
    
        
    <!-- Шаблон SINGLE PRODUCT -->
        <?php if ( is_product() ) : ?>
        <div class="containers">

            <div class="product__wrapper">
                <div class="catalog-left-block">
                    <?php
                    /* Показать сайдбар кроме страницы single-product, у корзины сайдбар скрыли в functions.php,
                    но его можно скрыть и здесь через условие !is_cart()*/
                        if(!is_single()) {
                                dynamic_sidebar('sidebar-left');
                    }
                        ?>
                </div>
    
                <div class="catalog-right-block" style="width: 100%;">
                    <?php  
                    /* Отобразить шаблон arrchive_products.php*/
                    if (is_singular( 'product' ) ) {
                        woocommerce_content();
                        } else{
                            woocommerce_get_template( 'archive-product.php' );
                        }
                                            ?>
                </div>
            </div>
        </div>
             <!-- Шаблон SHOP -->
            <?php elseif ( is_shop() ) : ?>
            <div class="containers">
        
            <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                        <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
                    <?php endif; ?>
                
        	        <div class="catalog__wrapper">
            	        <div class="catalog-left-block">
                            
                            <?php
                            /* Показать сайдбар кроме страницы single-product, у корзины сайдбар скрыли в functions.php,
                            но его можно скрыть и здесь через условие !is_cart()*/
                                if(!is_single()) {
                                     dynamic_sidebar('sidebar-left');
                                }
                             ?>
                        </div>
                        
                        <div class="catalog-right-block">
                            <?php  
                            /* Отобразить шаблон arrchive_products.php*/
                            if (is_singular( 'product' ) ) {
                                woocommerce_content();
                                } else{
                                    woocommerce_get_template( 'archive-product.php' );
                                }
                                                    ?>
                        </div>
                    </div> 
              </div>
        	<?php elseif ( is_cart() ) : ?>  

    
        <?php /*endif; */?>
        <!-- общий шаблон -->
        <?php else : ?>  
            <div class="containers">
                
                <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
                
                <div class="catalog__wrapper">
                    <div class="catalog-left-block">
                            
                            <?php
                            /* Показать сайдбар кроме страницы single-product, у корзины сайдбар скрыли в functions.php,
                            но его можно скрыть и здесь через условие !is_cart()*/
                                if(!is_single()) {
                                     dynamic_sidebar('sidebar-left');
                                }
                             ?>
                        </div>
                        
                        <div class="catalog-right-block">
                            <?php  
                            /* Отобразить шаблон arrchive_products.php*/
                            if (is_singular( 'product' ) ) {
                                woocommerce_content();
                                } else{
                                    woocommerce_get_template( 'archive-product.php' );
                                }
                                                    ?>
                        </div>
                    </div> 
                </div>
        
        <?php endif; ?>

                    
</main>

<?php get_footer(); ?>