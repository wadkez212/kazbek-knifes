<?php

/* добавляем поддержку woocommerce к теме */
add_theme_support( 'woocommerce' );

/* Отключаем zoom картинок в single product */
add_action('after_setup_theme', 'remove_zoom_theme_support', 100);
function remove_zoom_theme_support() {
    remove_theme_support('wc-product-gallery-zoom');
}

add_theme_support( 'woocommerce', array(
'gallery_thumbnail_image_width' => 100,
) );






	add_filter( 'upload_mimes', 'svg_upload_allow' );
	# Добавляет SVG в список разрешенных для загрузки файлов.
	function svg_upload_allow( $mimes ) {
		$mimes['svg']  = 'image/svg+xml';

		return $mimes;
	}


	add_filter( 'wp_check_filetype_and_ext', 'fix_svg_mime_type', 10, 5 );
	# Исправление MIME типа для SVG файлов.
	function fix_svg_mime_type( $data, $file, $filename, $mimes, $real_mime = '' ){

		// WP 5.1 +
		if( version_compare( $GLOBALS['wp_version'], '5.1.0', '>=' ) )
			$dosvg = in_array( $real_mime, [ 'image/svg', 'image/svg+xml' ] );
		else
			$dosvg = ( '.svg' === strtolower( substr($filename, -4) ) );

		// mime тип был обнулен, поправим его
		// а также проверим право пользователя
		if( $dosvg ){

			// разрешим
			if( current_user_can('manage_options') ){

				$data['ext']  = 'svg';
				$data['type'] = 'image/svg+xml';
			}
			// запретим
			else {
				$data['ext'] = $type_and_ext['type'] = false;
			}

		}

		return $data;
	}




	if( function_exists('acf_add_options_page') ) {
	
		acf_add_options_page();
		acf_add_options_sub_page('Header');
		acf_add_options_sub_page('Footer');
		
		acf_add_options_page(array(
		'page_title' 	=> 'Основные настройки',
		'menu_title'	=> 'Настройки темы',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
		
		acf_add_options_sub_page(array(
		'page_title' 	=> 'Настройки шапки',
		'menu_title'	=> 'header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Настройки подвала',
		'menu_title'	=> 'footer',
		'parent_slug'	=> 'theme-general-settings',
	));
		
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Post Settings',
		'menu_title'	=> 'Post Settings',
		'menu_slug' 	=> 'post-settings',
		'capability'	=> 'edit_posts',
		'parent_slug'	=> 'edit.php',
		'redirect'		=> false
	));
		
	
	}

    function my_theme_add_woocommerce_support() {
        add_theme_support('woocommerce');
    }
    add_action('after_setup_theme', 'my_theme_add_woocommerce_support');
    
    
    
    
    /* Для cart.php */
    add_theme_support('woocommerce');
    
    /* Убрать сайбар (sidebar cart) в cart.php и checkout.php*/
    add_action('wp_head', 'hide_sidebar' );
    function hide_sidebar(){ 
    if(is_cart() || is_checkout() ){ ?>
    
        <style type="text/css">
            #secondary {
                display: none;
            }
        </style>
<?php
    }
}

/* SINGLE PRODUCT ТАБЫ */
add_filter( 'woocommerce_product_tabs', 'wootabs_rename', 98 );

function wootabs_rename( $tabs ) {
    
    unset($tabs['additional_information']);
    $tabs['description']['title'];

	
	/*Добавил Вкладку "Доставка" к товару*/
	    $tabs['new_tab_1'] = array(
        'title'     => 'Доставка',
        'priority'     => 50,
        'callback'     => 'tab_dostavka_content'
    );
    
    $tabs['new_tab_2'] = array(
        'title'     => 'Оплата',
        'priority'     => 50,
        'callback'     => 'tab_oplata_content'
    );
    
    $tabs['new_tab_3'] = array(
        'title'     => 'Гарантия и возврат',
        'priority'     => 50,
        'callback'     => 'tab_garant_vozvrat_content'
    );
    return $tabs;
}
/* Содержимое вкладки-таба Доставка */
function tab_dostavka_content() {
    echo '<p>Наш интернет-магазин предлагает несколько вариантов доставки.</p>
	<h3>Доставка по СПБ</h3>
	<ul>
	<li class="tab_dostavka_oplata">Доставку курьером стоимостью 300 рублей.</li>
	<li class="tab_dostavka_oplata">По будням с 10:00 до 18:00, предварительно позвонив по телефону: <a href="tel:79111111111">+7 (911) 111-11-11</a>. Оплата: наличными. Или через банк</li>
	<li class="tab_dostavka_oplata">Бесплатная доставка от 50000 рублей суммы заказа.</li>
	</ul>
	<h3>Доставка по России</h3>
	<ul>
	<li class="tab_dostavka_oplata">Доставка службой СДЭК. Стоимость и срок доставки зависят от объема заказа и города, в который доставляется заказ.</li>
	<li class="tab_dostavka_oplata">Доставка службой ДЕЛОВЫЕ ЛИНИИ. Стоимость и срок доставки зависят от объема заказа и города, в который доставляется заказ.</li>
	<li class="tab_dostavka_oplata">Доставка Почтой России. Стоимость и срок доставки зависят от объема заказа и города, в который доставляется заказ.</li>
	</ul>';
}

/* Содержимое вкладки-таба оплата */
function tab_oplata_content() {
    echo '<p>Наш интернет-магазин предлагает несколько вариантов оплаты.</p>';
}

function tab_garant_vozvrat_content() {
    echo '<p>Вы можете сделать возврать товара, если:</p>';
}

/* поле кол-во товара в cart.php */
/*function woocommerce_quantity_input( $args = array(), $product = null, $echo = true ) {
 
	if ( is_null( $product ) ) {
		$product = $GLOBALS[ 'product' ];
	}
 
	// значения по умолчанию
	$defaults = array(
		'input_name' => 'quantity',
		'input_value' => '1',
		'max_value' => apply_filters( 'woocommerce_quantity_input_max', -1, $product ),
		'min_value' => apply_filters( 'woocommerce_quantity_input_min', 0, $product ),
		'step' => 1,
	);
 
	// этот фильтр-хук не убирать
	$args = apply_filters( 'woocommerce_quantity_input_args', wp_parse_args( $args, $defaults ), $product );
 
	// немного валидаций
	$args[ 'min_value' ] = max( $args[ 'min_value' ], 0 );
	$args[ 'max_value' ] = 0 < $args[ 'max_value' ] ? $args[ 'max_value' ] : 20;
 
	if ( '' !== $args['max_value'] && $args[ 'max_value' ] < $args[ 'min_value' ] ) {
		$args['max_value'] = $args[ 'min_value' ];
	}
 
	$options = '';
 
	// запускаем цикл создания выпадающего селекта
	for ( $count = $args[ 'min_value' ]; $count <= $args[ 'max_value' ]; $count = $count + $args[ 'step' ] ) {
 
		// Cart item quantity defined?
		if ( '' !== $args[ 'input_value' ] && $args[ 'input_value' ] >= 1 && $count == $args[ 'input_value' ] ) {
			$selected = 'selected';
		} else {
			$selected = '';
		}
 
		$options .= '<option value="' . $count . '"' . $selected . '>' . $count . '</option>';
 
	}
 
	$html = '<div class="quantity"><select name="' . $args[ 'input_name' ] . '">' . $options . '</select></div>';
 
	if ( $echo ) {
		echo $html;
	} else {
		return $html;
	}
}*/


// Кнопки плюс/минус для добавления количества на странице товара и в корзине.
add_action( 'woocommerce_before_quantity_input_field', 'quantity_plus', 25 );
add_action( 'woocommerce_after_quantity_input_field', 'quantity_minus', 25 );
 
function quantity_plus() {
	echo '<img class="plus" src="' . get_site_url() . '/wp-content/uploads/2022/07/icons__cart-product-plus.png"></img>';
}
 
function quantity_minus() {
	echo '<img class="minus" src="' . get_site_url() . '/wp-content/uploads/2022/07/icons__cart-product-minus.png"></img>';
}
 
// 2. jQuery script для кнопок.
add_action( 'wp_footer', 'my_quantity_plus_minus' );
 
function my_quantity_plus_minus() {
 
   if ( ! is_product() && ! is_cart() ) return;
   ?>
      <script type="text/javascript">
 
      jQuery( function( $ ) {   
 
         $(document).on( 'click', '.plus, .minus', function() {
 
         var qty = $( this ).parent( '.quantity' ).find( '.qty' );
         var val = parseFloat(qty.val());
         var max = parseFloat(qty.attr( 'max' ));
         var min = parseFloat(qty.attr( 'min' ));
         var step = parseFloat(qty.attr( 'step' ));
 
         if ( $( this ).is( '.plus' ) ) {
            if ( max && ( max <= val ) ) {
               qty.val( max ).change();
            } else {
               qty.val( val + step ).change();
            }
         } else {
            if ( min && ( min >= val ) ) {
               qty.val( min ).change();
            } else if ( val > 0 ) {
               qty.val( val - step ).change();
            }
         }
       });
 });
       </script>
   <?php
}

/* Автоматическое обновление корзины в зависимости от изменения количества товара в корзине cart.php*/
add_action( 'wp_footer', 'cart_update_qty_script' );
function cart_update_qty_script() {
    if (is_cart()) :
    ?>
    <script>
        jQuery('div.woocommerce').on('change', '.qty', function(){
            jQuery("[name='update_cart']").removeAttr('disabled');
            jQuery("[name='update_cart']").trigger("click");
        });
    </script>
    <?php

endif;
}


/* Автоматическое обновление кол-ва товара в header рядом с иконкой корзины*/
add_filter('woocommerce_add_to_cart_fragments', 'header_add_to_cart_fragment');
function header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;
    ob_start();
    ?>
    <span class="basket-btn__counter"><?php echo sprintf($woocommerce->cart->cart_contents_count); ?></span>
    <?php
    $fragments['.basket-btn__counter'] = ob_get_clean();
    return $fragments;
}






/* Переименовываем поля в chekcout */
add_filter( 'woocommerce_checkout_fields', 'truemisha_fio_field', 25 );
 
function truemisha_fio_field( $fields ) {
 
	$fields[ 'order' ][ 'order_comments' ][ 'label' ] = 'Комментарий';
	$fields['order']['order_comments']['placeholder'] = '';
 
	return $fields;
 
}



/* SINGLE PRODUCT шорткод ВЫ СМОТРЕЛИ РАНЕЕ ЗАПОМИНАЕМ ТОВАР В КУКИ */ 
add_action( 'template_redirect', 'recently_viewed_product_cookie', 20 );
 
function recently_viewed_product_cookie() {
 
	// если находимся не на странице товара, ничего не делаем
	if ( ! is_product() ) {
		return;
	}
 
 
	if ( empty( $_COOKIE[ 'woocommerce_recently_viewed_2' ] ) ) {
		$viewed_products = array();
	} else {
		$viewed_products = (array) explode( '|', $_COOKIE[ 'woocommerce_recently_viewed_2' ] );
	}
 
	// добавляем в массив текущий товар
	if ( ! in_array( get_the_ID(), $viewed_products ) ) {
		$viewed_products[] = get_the_ID();
	}
 
	// нет смысла хранить там бесконечное количество товаров
	if ( sizeof( $viewed_products ) > 15 ) {
		array_shift( $viewed_products ); // выкидываем первый элемент
	}
 
 	// устанавливаем в куки
	wc_setcookie( 'woocommerce_recently_viewed_2', join( '|', $viewed_products ) );
 
}

/* SINGLE PRODUCT шорткод ВЫ СМОТРЕЛИ РАНЕЕ, формируем ШОРТКОД и прописываем columns=4*/
add_shortcode( 'recently_viewed_products', 'truemisha_recently_viewed_products' );
function truemisha_recently_viewed_products() {
 
	if( empty( $_COOKIE[ 'woocommerce_recently_viewed_2' ] ) ) {
		$viewed_products = array();
	} else {
		$viewed_products = (array) explode( '|', $_COOKIE[ 'woocommerce_recently_viewed_2' ] );
	}
 
	if ( empty( $viewed_products ) ) {
		return;
	}
 
	// надо ведь сначала отображать последние просмотренные
	$viewed_products = array_reverse( array_map( 'absint', $viewed_products ) );
 
	$title = '<h3>Вы уже смотрели</h3>';
 
	$product_ids = join( ",", $viewed_products );
 
	return  do_shortcode( "[products ids='$product_ids' columns='4']" );
}

add_action('wp_ajax_add_compare'       , 'add_compare');
add_action('wp_ajax_nopriv_add_compare', 'add_compare');
add_action('wp_ajax_remove_compare'       , 'remove_compare');
add_action('wp_ajax_nopriv_remove_compare', 'remove_compare');
function add_compare($id){
    session_start();
    if (!$_SESSION['compare']){
        $_SESSION['compare'] = [];
    }
    if(array_search($_REQUEST['product_id'],$_SESSION['compare'])){
        wp_die();
    }else{
        echo(json_encode( array('status'=>'ok','request_vars'=>$_REQUEST['product_id']) ));
        array_push($_SESSION['compare'], $_REQUEST['product_id']);
        echo(json_encode( array('status'=>$_SESSION['compare']) ));
        wp_die();
    }

}

function remove_compare($id){
    session_start();
    $key = array_search($_REQUEST['product_id'],$_SESSION['compare']);
    unset($_SESSION['compare'][$key]);
    echo(json_encode( array('status'=>$key)));
    wp_die();
}


function js_variables(){
    $variables = array (
        'ajax_url' => admin_url('admin-ajax.php'),
        'is_mobile' => wp_is_mobile()
    );
    echo '<script type="text/javascript">window.wp_data = '.json_encode($variables).'</script>';

}
add_action('wp_head','js_variables');



function count_compare(){
    return 2;
}


?>