<?php get_header(); ?>

<main id="primary" class="site-main">
    <div class="containers">
        <?php woocommerce_breadcrumb(); ?>
            
            <!-- ПЕРЕБИРАЕМ ШАБЛОНЫ -->
            <!-- Если шаблон магазина -->
           <?php if(is_shop()) { ?>
           	    <!-- Заголовок shop -->
    	       <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                    <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
                <?php endif; ?>
            
    	        <div class="catalog__wrapper">
        	        <div class="catalog-left-block">
                        
                        <?php
                        /* Показать сайдбар кроме страницы single-product, у корзины сайдбар скрыли в functions.php,
                        но его можно скрыть и здесь через условие !is_cart()*/
                            if(!is_single()) {
                                 dynamic_sidebar('sidebar-left');
                            }
                         ?>
                    </div>
                    
                    <div class="catalog-right-block">
                        <?php  
                        /* Отобразить шаблон arrchive_products.php*/
                        if (is_singular( 'product' ) ) {
                            woocommerce_content();
                            } else{
                                woocommerce_get_template( 'archive-product.php' );
                            }
                                                ?>
                    </div>
                </div> 
                
                <!-- Если одиночный товар -->
            <? } else if(is_single()) { ?>
            
                <!-- Оборачиваем в product wrapper, а не в catalog__wrapper, тк в catalog__wrapper у нас уже прописан
                display: flex, а в single product flex нам не нужен, иначе блок похожие товара поплывет нах -->
                <div class="product__wrapper">
        	        <div class="catalog-left-block">
                        <?php
                        /* Показать сайдбар кроме страницы single-product, у корзины сайдбар скрыли в functions.php,
                        но его можно скрыть и здесь через условие !is_cart()*/
                            if(!is_single()) {
                                 dynamic_sidebar('sidebar-left');
                        }
                         ?>
                    </div>
                    
                    <div class="catalog-right-block">
                        <?php  
                        /* Отобразить шаблон arrchive_products.php*/
                        if (is_singular( 'product' ) ) {
                            woocommerce_content();
                            } else{
                                woocommerce_get_template( 'archive-product.php' );
                            }
                                                ?>
                    </div>
                </div>
            <!-- третий шаблон -->
            <? } else {?>
            <!-- Заголовок shop -->
                <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                    <h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
                <?php endif; ?>
                
                <div class="catalog__wrapper">
        	        <div class="catalog-left-block">
                        <?php
                        /* Показать сайдбар кроме страницы single-product, у корзины сайдбар скрыли в functions.php,
                        но его можно скрыть и здесь через условие !is_cart()*/
                            if(!is_single()) {
                                 dynamic_sidebar('sidebar-left');
                        }
                         ?>
                    </div>
                    
                    <div class="catalog-right-block">
                        <?php  
                        /* Отобразить шаблон arrchive_products.php*/
                        if (is_singular( 'product' ) ) {
                            woocommerce_content();
                            } else{
                                woocommerce_get_template( 'archive-product.php' );
                            }
                                                ?>
                    </div>
                </div>
            <? } ?>
                    
</main>

<?php get_footer(); ?>