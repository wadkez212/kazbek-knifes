<?php namespace Premmerce\Filter\Admin\Tabs;

use Premmerce\Filter\Admin\Tabs\Base\BaseSettings;

class SeoSettings extends BaseSettings
{


    /**
     * @var string
     */
    protected $page = 'premmerce-filter-admin-seo-settings';

    /**
     * @var string
     */
    protected $group = 'premmerce_filter-seo-settings';

    /**
     * @var string
     */
    protected $optionName = 'premmerce_filter_seo_settings';

    /**
     * Register hooks
     */
    public function init()
    {
        add_action('admin_init', [$this, 'initSettings']);
    }

    /**
     * Init settings
     */
    public function initSettings()
    {
        add_action('premmerce_filter_settings_after_textarea_callback', [$this, 'renderVariableButtons']);
        add_action('premmerce_filter_settings_after_input_callback', [$this, 'renderVariableButtons']);
        add_action('premmerce_filter_settings_after_text_editor_callback', [$this, 'renderVariableButtons']);

        register_setting($this->group, $this->optionName);


        $settings = [
            'default' => [
                'label'  => __('Default', 'premmerce-filter'),
                'fields' => [
                    'use_default_settings' => [
                        'type'  => 'checkbox',
                        'label' => __('Use default seo settings', 'premmerce-filter'),
                    ],
                ],
            ],
            'meta'    => [
                'label'    => __('Default Metadata', 'premmerce-filter'),
                'callback' => function () {
                    $sectionDescription = __(
                        'This settings are used to setup default meta data for filter pages that are not defined in rules',
                        'premmerce-filter'
                    );
                    print("<p>{$sectionDescription}</p>");
                },
                'fields'   => [
                    'h1'               => [
                        'type'  => 'text',
                        'title' => __('H1', 'premmerce-filter'),
                        'id'    => 'rule-h1'
                    ],
                    'title'            => [
                        'type'  => 'text',
                        'title' => __('Document title', 'premmerce-filter'),
                        'id'    => 'rule-title'
                    ],
                    'meta_description' => [
                        'type'  => 'textarea',
                        'title' => __('Meta description', 'premmerce-filter'),
                        'id'    => 'rule-meta-description'
                    ],
                    'description'      => [
                        'type'  => 'editor',
                        'title' => __('Category description', 'premmerce-filter'),
                        'id'    => 'rule-description'
                    ],
                ],
            ]
        ];

        $this->registerSettings($settings, $this->page, $this->optionName);
    }

    /**
     * @param array $args
     */
    public function renderVariableButtons($args)
    {
        if (isset($args['id']) && in_array(
                $args['id'],
                ['rule-h1', 'rule-title', 'rule-meta-description', 'rule-description']
            )) {
            premmerce_filter_admin_variables('#' . $args['id']);
        }
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return __('SEO Settings', 'premmerce-filter');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'seo_settings';
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return true;
    }


}
