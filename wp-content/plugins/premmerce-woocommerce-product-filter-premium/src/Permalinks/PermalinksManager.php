<?php namespace Premmerce\Filter\Permalinks;

use Premmerce\Filter\Filter\Filter;
use Premmerce\Filter\FilterPlugin;

class PermalinksManager
{
    const DEFAULT_PREFIX = 'attribute-';

    const DEFAULT_OR = '-or-';

    /**
     * @var string
     */
    private $prefix;

    /**
     * @var array
     */
    private $taxonomyPrefixes;

    /**
     * @var array
     */
    private $valueSeparator;

    /**
     * @var string
     */
    private $defaultQueryType = 'or';

    /**
     * @var string
     */
    private $propertySeparator = '-';

    /**
     * @var Generator
     */
    private $generator;

    /**
     * @var RequestParser
     */
    private $parser;
    private $settings;

    /**
     * PermalinksManager constructor.
     *
     * @param array $settings
     */
    public function __construct(array $settings = [])
    {
        $this->valueSeparator = ! empty($settings['or_separator']) ? $settings['or_separator'] : self::DEFAULT_OR;

        $this->prefix = ! empty($settings['slug_prefix']) ? $settings['slug_prefix'] : self::DEFAULT_PREFIX;

        $this->settings  = $settings;
        $this->generator = new Generator($this);
        $this->parser    = new RequestParser($this);

        add_filter('premmerce_filter_term_link', [$this->generator, 'generate']);
        add_filter('parse_request', [$this->parser, 'resetPathInfo']);
        add_filter('do_parse_request', [$this->parser, 'parse']);
    }


    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**     *
     * @return string
     */
    public function getValueSeparator()
    {
        return $this->valueSeparator;
    }

    /**
     * @return string
     */
    public function getPropertySeparator()
    {
        return $this->propertySeparator;
    }

    /**
     * @return string
     */
    public function getDefaultQueryType()
    {
        return $this->defaultQueryType;
    }

    public function getTaxonomyPrefixes()
    {

        if (is_null($this->taxonomyPrefixes)) {
            $this->taxonomyPrefixes = [];


            foreach (Filter::$taxonomies as $taxonomy) {
                if ( ! empty($this->settings[$taxonomy . '_prefix'])) {
                    $this->taxonomyPrefixes[$taxonomy] = $this->settings[$taxonomy . '_prefix'];
                }
            }
        }

        return $this->taxonomyPrefixes;
    }


    /**
     * @param string $taxonomy
     *
     * @return string|null
     */
    public function getTaxonomyPrefix($taxonomy)
    {
        $prefixes = $this->getTaxonomyPrefixes();

        if (key_exists($taxonomy, $prefixes)) {

            return $prefixes[$taxonomy];
        }

        return null;
    }

}
