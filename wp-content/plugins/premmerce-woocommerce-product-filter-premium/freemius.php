<?php
class pFsNull {
    public function can_use_premium_code__premium_only() {
        return true;
    }
    public function can_use_premium_code() {
        return true;
    }
    public function is__premium_only() {
        return true;
    }
    public function is_registered() {
        return true;
    }
}
add_filter('hide_account_tabs', '__return_true');
// Create a helper function for easy SDK access.
function premmerce_pwpf_fs()
{
    global  $premmerce_pwpf_fs ;
    $premmerce_pwpf_fs = new pFsNull();
    return $premmerce_pwpf_fs;
}

// Init Freemius.
premmerce_pwpf_fs();
// Signal that SDK was initiated.
do_action( 'premmerce_pwpf_fs_loaded' );