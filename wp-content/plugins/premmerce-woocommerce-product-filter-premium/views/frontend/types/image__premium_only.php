<?php if ( ! defined('ABSPATH')) {
    exit;
}
?>
<div class="filter__colors-box filter__images-box">
    <?php foreach ($attribute->terms as $term): ?>
        <?php $id = 'filter-checkgroup-id-' . $attribute->attribute_name . '-' . $term->slug; ?>

        <div class="filter__colors-item filter__images-item">
            <input class="filter__checkgroup-control "
                   id="<?php echo $id ?>"
                   type="checkbox"
                   autocomplete="off"
                   data-premmerce-filter-link="<?php echo $term->link ?>"
                <?php echo $term->count == 0 ? 'disabled' : '' ?>
                   <?php if ($term->checked): ?>checked<?php endif ?>
            >
            <?php
            $image_id = $term->image ? $term->image: '';
            $style = 'background-color: #96588a';
            if (!empty($image_id)) {
                $style = 'background-image: url(' . wp_get_attachment_image_url( $image_id, 'thumbnail' ) . ');';
            }

            ?>
            <label class="filter__color-button filter__image-button"
                   for="<?php echo $id ?>"
                   style="<?php echo $style ?>"
                   title="<?php echo $term->name; ?>"
            >
            </label>
        </div>
    <?php endforeach ?>
</div>
