��    2      �  C   <      H  	   I  	   S  	   ]  	   g  	   q      {  
   �  �   �  .   +     Z     j  �  �     0     9     >     E     Q     e     ~     �  
   �     �  	   �     �  (   �  #   	     0	     E	     S	      l	     �	  #   �	     �	  '   �	     

     
      
     &
     5
     I
     d
  
   ~
     �
     �
     �
  ?   �
     �
          *  t  ?     �     �     �     �     �  .   	     8    T  V   \     �     �  �  �     ^     g     |     �  :   �  @   �  :        M  "   `     �  	   �  !   �  N   �  A     :   C     ~  5   �  B   �  2     J   G  >   �  n   �  (   @     i     o     �  (   �  9   �  0        8     T     \     i  ?   �     �     �  
   �         	              %      0   $   '       -                           /                    )                    &                                 
       2             ,   (   1      #   "         *          !          +            .    1 Widgets 2 Widgets 3 Widgets 4 Widgets 6 Widgets Add widgets to appear in Header. Contact us Display a product slider and product categories in front page template. Slider, navigation widget and more options, Go Pro version. Display product navigation through categories. Easy Storefront Easy Storefront Theme Easy Storefront is a responsive and fully customizable template with tons of features. The theme can be used to develop business, WooCommerce or Easy Digital download shopping cart - store front, portfolio and others web sites. The Flexibility and Theme Options such as WooCommerce integration, Product Slider, Product Grid, Header & footer Customization, typography features provided in theme will amaze you. Looking for a Feature rich Store front theme? Look no further! See the demo, The only theme you will ever need: https://wordpress.org/themes/easy-storefront/ For more information about Easy Storefront please go to https://www.ceylonthemes.com/product/wordpress-storefront-theme Facebook Fade Footer Google-Plus Home Header Widgets Home Page Header Widgets Max Items to Show: More details My Account Open Sans font: on or offon Pinterest Product Categories Product Categories to show in Navigation Product Slider, Navigation & Banner See Premium Features Select Banner Select Number of Widgets Select Posts with featured image Select Product Category Select Products with featured image Select Top banner (Page) Show Home Product Slider and Navigation Skip to content Skype Slide Slider Effects Slider Height (Max) Slider animation speed(ms) THEME: Product Navigation Top Banner Twitter ceylonthemes https://www.ceylonthemes.com/ https://www.ceylonthemes.com/product/wordpress-storefront-theme labelSearch for: placeholderSearch &hellip; submit buttonSearch PO-Revision-Date: 2022-01-10 11:30:24+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: ru
Project-Id-Version: Themes - Easy Storefront
 1 Виджет 2 Виджета 3 Виджета 4 Виджета 6 Виджетов Добавить виджеты в Шапку. Обратная связь Показывать Слайдер с Товарами и Категории Товаров на шаблоне главной страницы. Слайдер, виджет навигации и больше опций, переход на Про версию.  Показывать навигатор товаров через категории.  Easy Storefront Тема Easy Storefront Easy Storefront - это гибкий и полностью настраиваемый шаблон со множеством возможностей. Эта тема может использоваться для создания и развития  сайтов для бизнеса, интернет магазинов на Woocommerce или Easy Digital, продающих сайтов, портфолио и т.п. Вас несомненно поразят гибкость и обилие настроек, таких как интеграция с Woocommerce, Слайдер Товаров, Сетка Товаров, настройка Шапки и Подвала и настройка шрифтов. Ищете многофункциональную тему для интернет магазина? Больше не ищите! Посмотрите демо и вам понадобится только одна тема : https://wordpress.org/themes/easy-storefront/. Для более подробной информации о Easy Storefront зайдите на https://www.ceylonthemes.com/product/wordpress-storefront-theme  Facebook Затемнение Подвал  Google-Plus Виджеты Шапки Главной Страницы  Виджеты для Шапки Главной Страницы Максимальное кол-во для показа : Подробнее Моя учётная запись on Pinterest Категории Товаров Категории Товаров для показа в Навигаторе  Слайдер Товаров, Навигация и Баннер Посмотреть Премиум Возможности Выбрать Баннер  Выбрать Количество Виджетов  Выбрать записи с изображение записи Выберите категорию товаров Выберите продукты с изображением записи Выбрать Верхний баннер (Страница)  Показывать Слайдер и Навигацию Товаров на Главной странице  Перейти к содержимому Skype Скольжение Эффекты слайдера Высота слайдера (макс) Скорость анимации слайдера (мс) ТЕМА: Навигатор по Товарам Верхний Баннер Twitter ceylonthemes https://www.ceylonthemes.com/ https://www.ceylonthemes.com/product/wordpress-storefront-theme Поиск : Поиск и помощь Поиск 